using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;
using SimpleJSON;
using static Spell;

public class SpellParser : MonoBehaviour {

    public static List<Spell> parseFile(string filePath) {
        string json = File.ReadAllText(filePath);
        var blob = JSON.Parse(json);

        var output = new List<Spell>();
        var spells = blob["spells"].Values;
        foreach (var spell in spells) {
            string name = spell.GetValueOrDefault("name", "MISSING");
            string descr = spell.GetValueOrDefault("description", "MISSING");
            string schoolStr = spell.GetValueOrDefault("school", "Fire");
            string vfx = spell.GetValueOrDefault("visualEffect", "ProjectileArcingFire");
            string dmgTypeStr = spell.GetValueOrDefault("damageType", "Fire");
            // can be one of: Any, Enemy, EmptyHex, Settlement, Player, <TerrainType>
            string validTarget = spell.GetValueOrDefault("validTarget", "Any");
            int minDmg = spell.GetValueOrDefault("minDamage", "0").AsInt;
            int maxDmg = spell.GetValueOrDefault("maxDamage", "0").AsInt;
            int aoe = spell.GetValueOrDefault("areaOfEffect", "0").AsInt;
            int range = spell.GetValueOrDefault("range", "0").AsInt;
            int mana = spell.GetValueOrDefault("manaCost", "0").AsInt;
            // TODO replace with Enum.TryParse and default values
            School school = (School) Enum.Parse(typeof(School), schoolStr);
            DamageType dmgType = (DamageType) Enum.Parse(typeof(DamageType), dmgTypeStr);
            output.Add(new Spell(name, descr, school, vfx, dmgType, validTarget,
                minDmg, maxDmg, aoe, range, mana));
        }

        return output;
    }
}
