using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public interface SpellEffect {
    public bool isMultiTurn();

    public Action<bool> getActivationEffect();

    public Action<bool> getDeactivationEffect();
}
