using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class HighlightOption : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {
    private Text text;
    private Color normalColor;
    private Color highlightColor;

    void Start() {
        text = GetComponentInChildren<Text>();
        normalColor = text.color;
        highlightColor = new Color(1, 1, 1);
    }

    public void OnPointerEnter(PointerEventData eventData) {
        // Debug.Log("Mouse enter");
        text.color = highlightColor;
    }
 
    public void OnPointerExit(PointerEventData eventData) {
        // Debug.Log("Mouse exit");
        text.color = normalColor;
    }
}
