using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#pragma warning disable 0649
public class DialogueNodeVisitedTracker : MonoBehaviour {
    
#pragma warning disable 0649
    [SerializeField] Yarn.Unity.DialogueRunner dialogueRunner;
#pragma warning restore 0649
    
    private HashSet<string> _visitedNodes = new HashSet<string>();

    void Start() {
        dialogueRunner.AddFunction("visited", 1, delegate (Yarn.Value[] parameters)
        {
            var nodeName = parameters[0];
            return _visitedNodes.Contains(nodeName.AsString);
        });
    }

    // Called by the Dialogue Runner to notify us that a node finished
    // running. 
    public void NodeComplete(string nodeName) {
        // Log that the node has been run.
        Debug.Log($"Completed node '{nodeName}'");
        _visitedNodes.Add(nodeName);
    }

	// Called by the Dialogue Runner to notify us that a new node 
	// started running. 
	public void NodeStart(string nodeName) {
        Debug.Log($"Getting the tags for node '{nodeName}'");
        var tags = new List<string>(dialogueRunner.GetTagsForNode(nodeName));
        
		Debug.Log($"Starting the execution of node {nodeName} with {tags.Count} tags.");
	}

    public void DialogueComplete() {
        Debug.Log($"Dialogue completed");
    }
}
