﻿using UnityEngine;

public static class HexMetrics {

	public const float outerRadius = 10f;

	public const float innerRadius = outerRadius * 0.866025404f;

	public static Vector3[] corners = {
		new Vector3(0f, 0f, outerRadius),
		new Vector3(innerRadius, 0f, 0.5f * outerRadius),
		new Vector3(innerRadius, 0f, -0.5f * outerRadius),
		new Vector3(0f, 0f, -outerRadius),
		new Vector3(-innerRadius, 0f, -0.5f * outerRadius),
		new Vector3(-innerRadius, 0f, 0.5f * outerRadius),
		new Vector3(0f, 0f, outerRadius)
	};

	public static (int, int)[] evenNeighborCoordinateDiffs = {
		(-1,1),  //top left
		(0,1),   //top right
		(-1,0),  //center left
		(1,0),   //center right
		(-1,-1), //bottom left
		(0,-1)   // bottom right
	};

	public static (int, int)[] oddNeighborCoordinateDiffs = {
		(0,1),  //top left
		(1,1),  //top right
		(-1,0), //center left
		(1,0),  //center right
		(0,-1), //bottom left
		(1,-1)  // bottom right
	};
}