using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightningFlashes : MonoBehaviour {
    private new Light light;

    void Start() {
        light = this.GetComponent<Light>();
        StartCoroutine(flashLightning());
    }

    IEnumerator flashLightning() {
        float startTime = Time.fixedUnscaledTime;
        yield return new WaitForSecondsRealtime(3f);
        bool animationOngoing = true;
        while (animationOngoing) {
            if (Random.Range(1, 100) <= 30) {
                light.enabled = true;
                yield return new WaitForSeconds(0.2f);
                light.enabled = false;
            }
            yield return new WaitForSeconds(Random.Range(0.5f, 3.5f));
            if (Time.fixedUnscaledTime - startTime >= 16f) {
                animationOngoing = false;
            }
        }
    }

    void Update() {
    }
}
