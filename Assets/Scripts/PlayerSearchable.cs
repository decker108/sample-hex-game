public interface PlayerSearchable {
    Player getActivePlayer();
}