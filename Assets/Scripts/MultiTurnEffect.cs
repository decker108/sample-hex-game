using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class MultiTurnEffect : SpellEffect {
    private Action<bool> activationEffect;
    private Action<bool> deactivationEffect;
    private int duration;

    public MultiTurnEffect(int duration, Action<bool> activation, Action<bool> deactivation) {
        this.duration = duration;
        this.activationEffect = activation;
        this.deactivationEffect = deactivation;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public bool isMultiTurn() {
        return true;
    }

    public Action<bool> getActivationEffect() {
        return activationEffect;
    }

    public Action<bool> getDeactivationEffect() {
        return deactivationEffect;
    }
}
