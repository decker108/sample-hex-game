using System.Collections;
using UnityEngine;
using System;

public class VfxManager: MonoBehaviour {

    private PlayerSearchable turnManager;
    public GameObject fireballPrefab;
    public GameObject blizzardPrefab;
    public GameObject lightningPrefab;
    public GameObject skyfallPrefab;

    public void Start() {
        turnManager = Transform.FindObjectOfType<TurnManager>();
    }

    public IEnumerator animateFireball(HexCell hex, Action onFinishedFn) {
        Vector3 start = turnManager.getActivePlayer().transform.position;
        Vector3 dest = hex.transform.position;
        Vector3 direction = (start - dest).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(direction);
        var obj = Instantiate(fireballPrefab, start, lookRotation);
        // move effect along parabolic path until destination
        float steps = 100;
        float maxH = 20;
        float midT = steps/2;
        for (float t = 0; t < steps; t++) {
            var pos = Vector3.Slerp(start, dest, t/(steps-1f));
            float tDiff = t - midT;
            float y = maxH - maxH * (tDiff * tDiff) / (midT * midT);
            Debug.Log($"y: {y} t: {t}");
            pos.y = y;
            obj.transform.position = pos;
            // TODO get next position and rotate fireball towards it
            // obj.transform.rotation = Quaternion.LookRotation((current - next).normalized)
            yield return new WaitForSeconds(0.01f);
            //Debug.Log($"Moving fireball to {pos}");
        }
        Debug.Log("Fireball at destination");
        onFinishedFn.Invoke();
        Destroy(obj);
    }

    public IEnumerator animateBlizzard(HexCell hex, Action onFinishedFn) {
        var gobj = Instantiate(blizzardPrefab, hex.transform.position + new Vector3(0, 15, 0), hex.transform.rotation);
        onFinishedFn.Invoke();
        yield return new WaitForSecondsRealtime(16);
        Destroy(gobj);
    }

    public IEnumerator animateLightning(HexCell hex, Action onFinishedFn) {
        var obj = Instantiate(lightningPrefab, hex.transform.position, hex.transform.rotation);
        yield return new WaitForSecondsRealtime(5);
        onFinishedFn.Invoke();
        yield return new WaitForSecondsRealtime(5);
        Destroy(obj);
    }

    public IEnumerator animateSkyfall(HexCell hex, Action onFinishedFn) {
        var gobj = Instantiate(skyfallPrefab, hex.transform.position + new Vector3(0, 15, 0), hex.transform.rotation);
        onFinishedFn.Invoke();
        yield return new WaitForSecondsRealtime(20);
        Destroy(gobj);
    }

    public void playAnimation(string vfxName, HexCell hex, Action onFinishedFn) {
        switch (vfxName) {
            case "ProjectileArcingFire": {
                StartCoroutine(animateFireball(hex, onFinishedFn));
                break;
            }
            case "VerticalIce": {
                StartCoroutine(animateBlizzard(hex, onFinishedFn));
                break;
            }
            case "VerticalLightning": {
                StartCoroutine(animateLightning(hex, onFinishedFn));
                break;
            }
            case "RainWater": {
                StartCoroutine(animateSkyfall(hex, onFinishedFn));
                break;
            }
            default: { // Using fireball as default for now
                Debug.Log($"No vfx match for {vfxName}, defaulting to fireball");
                StartCoroutine(animateFireball(hex, onFinishedFn));
                break;
            }
        }
    }
}
