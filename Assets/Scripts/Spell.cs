using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Spell {
    public enum School {
        Fire, Water, Earth, Air, Life, Death
    }

    public enum DamageType {
        Normal, Fire, Ice, Electricity, Water, Mana, Healing, Harmless
    }

    public string name;
    public string description;
    public School school; // TODO turn into list (for possible combos)
    public int level; // TODO required level of each school
    public string vfx; // TODO turn into object with multiple parameters
    public DamageType dmgType;
    public string validTarget;
    public int minDmg, maxDmg;
    public int aoe, range;
    public int manaCost;

    public Spell(string name,
            string description,
            School school,
            string vfx,
            DamageType dmgType,
            string validTarget,
            int minDmg,
            int maxDmg,
            int aoe,
            int range,
            int manaCost) {
        this.name = name;
        this.description = description;
        this.school = school;
        this.level = 1; // TODO get from config
        this.vfx = vfx;
        this.dmgType = dmgType;
        this.validTarget = validTarget;
        this.minDmg = minDmg;
        this.maxDmg = maxDmg;
        this.aoe = aoe;
        this.range = range;
        this.manaCost = manaCost;
    }

    public override string ToString() {
        return $"{name} {description} {school} {vfx}";
    }

    public bool isTargetValid(HexCell hex) {
        // TODO check other target types too: Enemy, EmptyHex, Settlement, Player
        if (this.validTarget == "Any") {
            return true;
        }
        // if not matching above, try terrain types
        return false;
    }

    public Action getActivationEffect(HexCell[] hexes, Player player) {
        // TODO figure out a way to make this generic and moddable
        if (dmgType == DamageType.Fire) {
            return () => {
                foreach (var hex in hexes) {
                    var terrain = hex.GetComponentInChildren<TerrainType>();
                    terrain.burnTerrain(hex);
                }
            };
        } else if (dmgType == DamageType.Ice) {
            return () => {
                foreach (var hex in hexes) {
                    var terrain = hex.GetComponentInChildren<TerrainType>();
                    terrain.freezeTerrain(hex);
                }
            };
        } else if (dmgType == DamageType.Electricity) {
            return () => {
                foreach (var hex in hexes) {
                    var terrain = hex.GetComponentInChildren<TerrainType>();
                    terrain.shockTerrain(hex);
                }
            };
        } else if (dmgType == DamageType.Water) {
            return () => {
                foreach (var hex in hexes) {
                    var terrain = hex.GetComponentInChildren<TerrainType>();
                    terrain.floodTerrain(hex);
                }
            };
        }
        return () => Debug.Log("Activation effect executed: NOOP");
    }
}
