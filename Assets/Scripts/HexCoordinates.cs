using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HexCoordinates {

	[SerializeField]
	private int x, z;
	public static int maxX, maxZ;

	public int X {
		get {
			return x;
		}
	}

	public int Z {
		get {
			return z;
		}
	}

	public HexCoordinates (int x, int z) {
		this.x = x;
		this.z = z;
	}

    public override string ToString() {
        return $"{x},{z}"; 
    }

	public static List<HexCoordinates> GetNeighbors(HexCoordinates coordinates) {
        List<HexCoordinates> output = new List<HexCoordinates>();
        bool isEven = coordinates.Z % 2 == 0;
        var offsets = isEven ? HexMetrics.evenNeighborCoordinateDiffs : HexMetrics.oddNeighborCoordinateDiffs;
        for (int i = 0; i < 6; i++) { // A hexagon always has six sides, so...
            int xOffset = offsets[i].Item1;
            int zOffset = offsets[i].Item2;
            int neighborX = coordinates.X + xOffset;
            int neighborZ = coordinates.Z + zOffset;
            if (neighborX < 0 || neighborZ < 0 || neighborX >= maxX || neighborZ >= maxZ) {
                continue;
            }
            output.Add(new HexCoordinates(neighborX, neighborZ));
        }
        return output;
    }

	public static List<HexCoordinates> GetNeighbors(int x, int z) {
		return GetNeighbors(new HexCoordinates(x, z));
	}

    public static HashSet<HexCoordinates> getConcentricHexes(HexCoordinates centerHex, int aoe) {
        var hexSet = new HashSet<HexCoordinates>();
        hexSet.Add(centerHex);
        for (int i = 0; i < aoe-1; i++) {
            var arr = new HexCoordinates[hexSet.Count];
            hexSet.CopyTo(arr);
            foreach (var hex in arr) {
                GetNeighbors(hex).ForEach(h => hexSet.Add(h));
            }
        }
        return hexSet;
    }

	public static void SetMapBounds(int x, int z) {
		maxX = x;
		maxZ = z;
	}

    public override bool Equals(object obj) {
		if (obj == null) {
			return false;
		}
		if (base.Equals(obj)) {
			return true;
		}
		HexCoordinates other = (HexCoordinates) obj;
		return other.X == this.x && other.Z == this.z;
    }

    public override int GetHashCode() {
        int hash = 17;
		hash = hash * 31 + x.GetHashCode();
		hash = hash * 31 + z.GetHashCode();
		return hash;
    }

	// Based on this SO answer: https://stackoverflow.com/a/5085274
	public float DistanceTo(HexCoordinates other) {
        float dx = this.X - other.X;
        float dy = this.Z - other.Z;

        if (Mathf.Sign(dx) == Mathf.Sign(dy)) {
            return Mathf.Abs(dx + dy);
        } else {
            return Mathf.Max(Mathf.Abs(dx), Mathf.Abs(dy));
        }
	}

    public static HexCoordinates getClosestPosition(HexCoordinates origin, List<HexCoordinates> positions) {
        if (positions.Count == 0) {
            return null;
        } else if (positions.Count == 1) {
            return positions[0];
        }
        var closest = (positions[0].DistanceTo(origin), positions[0]);
        foreach (var pos in positions) {
            var distance = pos.DistanceTo(origin);
            if (distance < closest.Item1) {
                closest = (distance, pos);
            }
        }
        return closest.Item2;
    }
}
