﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class HexCell : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler {
	[SerializeField]
    public HexCoordinates coordinates;

    public TerrainType.Name terrainType;
    public TerrainType.Status terrainStatus;
    private HexGrid hexGrid;
    private PlayerSearchable turnManager;

    private static Yarn.Unity.DialogueRunner dialogueRunner;
    private static Color RED = new Color(1,0,0.04753733f);
    private static Color ORANGE = new Color(1,0.5772549f,0);
    private static Color GREEN = new Color(0,0.7075472f,0.2244395f);

    void Start() {
        hexGrid = transform.parent.GetComponent<HexGrid>();
        turnManager = Transform.FindObjectOfType<TurnManager>();
        if (dialogueRunner == null) {
            dialogueRunner = FindObjectOfType<Yarn.Unity.DialogueRunner>();
        }
    }

    public async void OnPointerClick(PointerEventData data) {
        // TODO prevent click if player is moving
        var player = turnManager.getActivePlayer();
        if (player.activeSpell != null) {
            handleSpellCast(player);
        } else if (hexGrid.isNpcInTile(coordinates)) {
            // we have a non-hostile npc in this hex, start dialogue
            NpcDialogue npc = hexGrid.getNpcInTile(coordinates).GetComponent<NpcDialogue>();
            dialogueRunner.StartDialogue(npc.talkToNode);
            // dialogueRunner.variableStorage.SetValue("foo", "bar");
        } else if (hexGrid.isEnemyInTile(coordinates)) {
            // show battle confirmation dialog
            if (player.coordinates.DistanceTo(coordinates) == 1) {
                var enemies = EncounterGenerator.generateEncounter(); // TODO move somewhere else?
                GlobalGameData.setEnemiesInCombat(enemies);
                var loadingScreen = GameObject.Find("LoadingScreenCanvas");
                var battlePanel = GameObject.Find("PlayerUICanvas").transform.GetChild(11).gameObject;
                BattlePanelHandler.initialize(battlePanel, loadingScreen, enemies.Count, enemies[0].pluralName, player);
            } // show "out of range" message here
        } else {
            if (player.movementPoints > 0) {
                var startPos = hexGrid.GetHexForCoordinate(player.coordinates);
                HexCell[] path = PathFinding.FindPath(startPos, this, hexGrid);
                if (path.Length > 0) {
                    new List<HexCell>(PathFinding.previousPath).ForEach((HexCell hc) => hc.transform.GetChild(0).gameObject.SetActive(false));
                    new List<HexCell>(path).ForEach((HexCell hc) => hc.transform.GetChild(0).gameObject.SetActive(true));
                    PathFinding.previousPath = path;
                    // TODO limit path by movement points

                    Debug.Log("Starting move");
                    bool result = await player.move(path, (hex) => false); // TODO cancel move if player is interrupted
                    Debug.Log($"Done moving, result: {result}");
                }
            }
        }
    }

    public void OnPointerEnter(PointerEventData pointerEventData) {
        var player = turnManager.getActivePlayer();
        hexGrid.resetOutlines(); // reset previous hex cell outlines
        hexGrid.resetLabels();
        hexGrid.setHexDebugText($"Hex: {coordinates}\nTerrain: {TerrainType.getHumanReadableName(terrainType)}\nState: {TerrainType.getHumanReadableName(terrainStatus)}");

        if (dialogueRunner?.IsDialogueRunning ?? false) {
            return;
        }

        if (player.activeSpell != null) {
            HashSet<HexCoordinates> hexSet = 
                HexCoordinates.getConcentricHexes(this.coordinates, player.activeSpell.aoe);
            foreach (var hex in hexSet) {
                hexGrid.GetHexForCoordinate(hex)
                    .transform.GetChild(0).gameObject.SetActive(true);
            }
        }
        if (player.activeSpell == null) {
            var startPos = hexGrid.GetHexForCoordinate(player.coordinates);
            HexCell[] path = PathFinding.FindPath(startPos, this, hexGrid);
            if (path.Length > 0) {
                int steps = 0;
                foreach (var hc in path) {
                    var outline = hc.transform.GetChild(0);
                    outline.gameObject.SetActive(true);
                    if (steps <= player.movementPoints) {
                        outline.GetComponent<MeshRenderer>().material.color = GREEN;
                    } else {
                        outline.GetComponent<MeshRenderer>().material.color = ORANGE;
                    }
                    var label = transform.parent.GetChild(0).Find($"Hex label {hc.coordinates.X},{hc.coordinates.Z}");
                    label.GetComponent<Text>().text = $"{steps}";
                    label.GetComponent<Text>().enabled = true;
                    steps++;
                }
                PathFinding.previousPath = path;
            }
        }
    }

    public void SetRandomColor() {
		MeshRenderer r = this.GetComponent<MeshRenderer>();
		r.material.color = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f), 0f);
    }

    public void SetNonRandomColor(float r, float g, float b) {
		MeshRenderer renderer = this.GetComponent<MeshRenderer>();
		renderer.material.color = new Color(r, g, b, 0f);
    }

    public float DistanceTo(HexCell neighbor) {
        return this.coordinates.DistanceTo(neighbor.coordinates);
    }

    private void handleSpellCast(Player player) {
        if (player.activeSpell.isTargetValid(this)) {
            Debug.Log($"Triggering spell {player.activeSpell.name} on {coordinates}");
            
            HashSet<HexCoordinates> hexSet = 
                HexCoordinates.getConcentricHexes(this.coordinates, player.activeSpell.aoe);
            var affectedHexes = new HexCell[hexSet.Count];
            int i = 0;
            foreach (var hexCoord in hexSet) {
                affectedHexes[i++] = hexGrid.GetHexForCoordinate(hexCoord);
            }
            
            var vfxManager = GameObject.Find("VfxManager").GetComponent<VfxManager>();
            var effect = player.activeSpell.getActivationEffect(affectedHexes, player);
            vfxManager.playAnimation(player.activeSpell.vfx, this, effect);
            
            player.mana -= player.activeSpell.manaCost;
            player.activeSpell = null;
            hexGrid.resetOutlines();
        } else {
            Debug.Log("Invalid target for spell");
        }
    }
}