﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System;

public class HexGrid : MonoBehaviour, HexMapSearchable {

	private int width = 12;
	private int height = 12;

	public HexCell cellPrefab;
	public Text cellLabelPrefab;
	public HexCellOutline outlinePrefab;

	HexCell[] cells;
	public Dictionary<(int, int), HexCell> cellMap;
	public List<Player> players = new List<Player>();
	public List<Npc> npcs = new List<Npc>();
	public List<Enemy> enemies = new List<Enemy>();

	Canvas gridCanvas;
	private TerrainBuilder terrainBuilder;

	public Player playerPrefab;
	public Npc npcPrefab;
	public Enemy enemyPrefab;

	void Awake () {
		gridCanvas = GetComponentInChildren<Canvas>();
		terrainBuilder = GetComponentInChildren<TerrainBuilder>();

		cells = new HexCell[height * width];
		cellMap = new Dictionary<(int, int), HexCell>(height * width);
		HexCoordinates.SetMapBounds(height, width);

		// TODO load game data in parallel
		GlobalGameData.loadTerrainData();
		GlobalGameData.loadSpellData();
		GlobalGameData.loadEnemyData();
		GlobalGameData.loadEquipmentData();

		for (int z = 0, i = 0; z < height; z++) {
			for (int x = 0; x < width; x++) {
				HexCell cell = createCell(x, z, i++);

				if (GlobalGameData.terrainMapping.hexFeatures.ContainsKey($"{x},{z}")) {
					HexFeatureManager.create(x, z, this, createInstantiatorFn(cell), addHexFeatureToList);
				}
			}
		}

		GlobalGameData.players = players;
	}

	private HexCell createCell (int x, int z, int i) {
		Vector3 position;
		position.x = (x + z * 0.5f - z / 2) * (HexMetrics.innerRadius * 2f);
		position.y = 0f;
		position.z = z * (HexMetrics.outerRadius * 1.5f);

		HexCell cell = cells[i] = Instantiate<HexCell>(cellPrefab);
		cell.transform.SetParent(transform, false);
		cell.transform.localPosition = position;
		cell.coordinates = new HexCoordinates(x, z);
		cell.name = $"Hex {x},{z}";

		HexCellOutline outline = Instantiate<HexCellOutline>(
			outlinePrefab, cell.transform.position + new Vector3(0, 2, 0), 
			Quaternion.Euler(90, 0, 0), cell.transform);
		outline.gameObject.SetActive(false);

		var terrainType = Instantiate<TerrainType>(
			terrainBuilder.terrainPrefab, cell.transform.position + new Vector3(0, 1, 0), 
			Quaternion.Euler(0, 0, 0), cell.transform);
		var terrainName = (TerrainType.Name) GlobalGameData.terrainMapping.map[z][x];
		terrainType.renderTerrain(terrainName, terrainBuilder, cell);

		Text label = Instantiate<Text>(cellLabelPrefab);
		label.rectTransform.SetParent(gridCanvas.transform, false);
		label.rectTransform.anchoredPosition =
			new Vector2(position.x, position.z);
		label.text = "0";
		label.name = $"Hex label {x},{z}";
		label.enabled = false;

		cellMap[(x,z)] = cell;

		return cell;
	}

	private Func<Type,object> createInstantiatorFn(HexCell cell) {
		return (Type t) => {
			if (t == typeof(Player)) {
				return Instantiate<Player>(playerPrefab, cell.transform.position + new Vector3(0, 1, 0), playerPrefab.transform.rotation);
			} else if (t == typeof(Npc)) {
				return Instantiate<Npc>(npcPrefab, cell.transform.position + new Vector3(0, 1, 0), npcPrefab.transform.rotation);
			} else if (t == typeof(Enemy)) {
				return Instantiate<Enemy>(enemyPrefab, cell.transform.position + new Vector3(0, 1, 0), enemyPrefab.transform.rotation);
			}
			return null;
		};
	}

	private void addHexFeatureToList(object obj) {
		if (obj.GetType() == typeof(Player)) {
			players.Add((Player) obj);
		} else if (obj.GetType() == typeof(Npc)) {
			npcs.Add((Npc) obj);
		} else if (obj.GetType() == typeof(Enemy)) {
			enemies.Add((Enemy) obj);
		}
	}

	public List<HexCell> GetHexesForCoordinates(List<HexCoordinates> coords) {
		return coords.ConvertAll<HexCell>(GetHexForCoordinate);
	}

	public HexCell GetHexForCoordinate(HexCoordinates coord) {
		return cellMap[(coord.X, coord.Z)];
	}

	public void resetOutlines() {
		foreach (var hc in cells) {
			hc.transform.GetChild(0).gameObject.SetActive(false);
		}
	}

	public void resetLabels() {
		var labels = transform.GetChild(0).GetComponentsInChildren<Text>();
		foreach (var label in labels) {
			label.enabled = false;
		}
	}

	public bool isNpcInTile(HexCoordinates coordinates) {
		return npcs.Exists((npc) => npc.coordinates.Equals(coordinates));
	}

	public Npc getNpcInTile(HexCoordinates coordinates) {
		return npcs.Find((npc) => npc.coordinates.Equals(coordinates));
	}

	public bool isPlayerInTile(HexCoordinates coordinates) {
		return players.Find(p => p.coordinates.Equals(coordinates));
	}

	public bool isEnemyInTile(HexCoordinates coordinates) {
		return enemies.Find(p => p.coordinates.Equals(coordinates));
	}

	public void setHexDebugText(string text) {
		var uiText = GameObject.Find("PlayerUICanvas").transform.Find("DebugInfoPanel").GetChild(0);
		uiText.GetComponent<Text>().text = text;
	}
}