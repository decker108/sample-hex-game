using System;

public interface CombatEntity {
    // Marker interface for players and enemies in combat
    (int, bool) takeDamage(int damage);

    int doDamageRoll();

    int getHitChance();

    int getDodgeChange();

    bool isPlayer();
}