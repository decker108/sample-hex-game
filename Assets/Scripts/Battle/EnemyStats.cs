using System;

public class EnemyStats : ICloneable {
    public string name;
    public string pluralName;
    public string description;
    public string prefab;
    public int maxHealth;
    public int health;
    public int damageReduction;
    public int hitModifier;
    public int dodgeModifier;

    public EnemyStats(string name, string pluralName, string description, string prefab,
        int maxHealth, int damageReduction, int hitModifier, int dodgeModifier) {
        this.name = name;
        this.pluralName = pluralName;
        this.description = description;
        this.prefab = prefab;
        this.maxHealth = maxHealth;
        this.health = maxHealth;
        this.damageReduction = damageReduction;
        this.hitModifier = hitModifier;
        this.dodgeModifier = dodgeModifier;
    }

    public object Clone() {
        return new EnemyStats(this.name, this.pluralName, this.description, this.prefab,
        this.maxHealth, this.damageReduction, this.hitModifier, this.dodgeModifier);
    }
}