using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Threading.Tasks;

public class CombatMovable : MonoBehaviour {
    void Start() {
        
    }

    void Update() {
        
    }

    public async Task moveToPosition(Vector3 start, Vector3 end) {
        Vector3 direction = -((start - end).normalized);
        Quaternion lookRotation = Quaternion.LookRotation(direction);
        transform.rotation = lookRotation;
        int steps = 25;
        for (int j = 0; j <= steps; j++) {
            var x = Mathf.Lerp(start.x, end.x, (float)j/(float)steps);
            var z = Mathf.Lerp(start.z, end.z, (float)j/(float)steps);
            transform.position = new Vector3(x, transform.position.y, z);
            await Task.Delay(TimeSpan.FromSeconds(0.05f));
        }
    }
}
