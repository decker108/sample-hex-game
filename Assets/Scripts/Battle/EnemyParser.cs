using System.IO;
using SimpleJSON;
using System.Collections.Generic;

public class EnemyParser {
    public static List<EnemyStats> parseFile(string filePath) {
        string json = File.ReadAllText(filePath);
        var blob = JSON.Parse(json);
        
        var enemyStatsList = new List<EnemyStats>(2);
        foreach (var parsedItem in blob.Values) {
            string name = parsedItem.GetValueOrDefault("name", "missing");
            string pluralName = parsedItem.GetValueOrDefault("name", "missing");
            string description = parsedItem.GetValueOrDefault("description", "missing");
            string prefab = parsedItem.GetValueOrDefault("prefab", "missing");
            int maxHealth = parsedItem.GetValueOrDefault("maxHealth", 1).AsInt;
            int damageReduction = parsedItem.GetValueOrDefault("damageReduction", 1).AsInt;
            int hitModifier = parsedItem.GetValueOrDefault("hitModifier", 1).AsInt;
            int dodgeModifier = parsedItem.GetValueOrDefault("dodgeModifier", 1).AsInt;
            enemyStatsList.Add(new EnemyStats(name, pluralName, description, prefab, 
                maxHealth, damageReduction, hitModifier, dodgeModifier));
        }
        return enemyStatsList;
    }
}