using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading.Tasks;

public class EnemyCombatBehavior {
    private HostileCombatEntity parent;

    public EnemyCombatBehavior(HostileCombatEntity parent) {
        this.parent = parent;
    }

    public async Task doTurn(CombatTurnManager turnManager, List<Player> players, List<HostileCombatEntity> enemies) {
        // choose target to attack
        var target = chooseMeleeTarget(players);
        // attack with melee/ranged/spell
        await turnManager.handleAttack(parent, target);
        // end turn
        return;
    }

    private Player chooseMeleeTarget(List<Player> players) {
        Player playerWithLowestHealth = players[0];
        foreach (var player in players) {
            if (player.health < playerWithLowestHealth.health) {
                playerWithLowestHealth = player;
            }
        }
        return playerWithLowestHealth;
    }
}
