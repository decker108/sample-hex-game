using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(TMPro.TMP_Text))]
public class EnemyInfoPopupHandler : MonoBehaviour {

    void Start() {
    }

    void Update() {
        // Maybe give a fixed position next to entity instead?
        moveToMousePosition();
    }

    public void init(string text) {
        moveToMousePosition();
        GetComponent<TMPro.TMP_Text>().text = text;
    }

    private void moveToMousePosition() {
        RectTransform rt = (RectTransform) transform;
        transform.parent.position = Input.mousePosition +
            new Vector3(rt.rect.width/2-10, -rt.rect.height/2-35, 0);
    }
}
