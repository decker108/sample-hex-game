using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using System.Threading.Tasks;
using UnityEngine.SceneManagement;

public class CombatTurnManager : MonoBehaviour {
    public GameObject battleArea;
    public TurnOrderVisualizer turnOrderViz;
    public GameObject infoPopup;
    public Player playerPrefab;

    private Vector3[] playerPositions = {
        new Vector3(8.55000019f,0f,-0.899999976f),
        new Vector3(8.55000019f,0f,-3.17000008f),
        new Vector3(8.55000019f,0f,-5.38999987f),
    };

    private Vector3[] enemyPositions = {
        new Vector3(-6.3499999f,0f,-1.02999997f),
        new Vector3(-7.11999989f,0f,-3.19000006f),
        new Vector3(-7.36999989f,0f,-5.39999962f),
        new Vector3(-9.39999962f,0f,-1.51999998f),
        new Vector3(-10.1599998f,0f,-4.30000019f),
    };

    private List<Player> players;
    private List<HostileCombatEntity> enemies;
    private CombatEntity attacker, defender;
    private Queue<(int, CombatEntity)> turnOrderQueue;
    private bool isPlayerTurn = false;
    private Player activePlayer;

    async void Start() {
        // debug code, remove later
        // var player = new Player();
        // player.
        // GlobalGameData.players = new List<Player>(new Player[]{player});
        // GlobalGameData.loadEnemyData();
        // GlobalGameData.setEnemiesInCombat(EncounterGenerator.generateEncounter());
        // end of debug code
        players = await instantiatePlayers();
        enemies = await instantiateEnemies();

        turnOrderQueue = new Queue<(int, CombatEntity)>(players.Count + enemies.Count + 2);
        var entitiesInTurnOrder = determineInitiatives(players, enemies);
        for (int i = 0; i < 2; i++) {
            foreach (var entity in entitiesInTurnOrder) {
                turnOrderQueue.Enqueue((0, entity));
            }
            turnOrderQueue.Enqueue((i+2, null));
        }
        turnOrderViz.init(turnOrderQueue.ToArray());

        doCombatTurns(); // does not need to be awaited by Start
    }

    void Update() {}

    public async Task doCombatTurns() {
        var controls = Transform.FindObjectOfType<CombatControls>();
        while (turnOrderQueue.Count > 0) {
            var (turnMarker, combatEntity) = turnOrderQueue.Dequeue();
            if (turnMarker > 0) {
                turnOrderQueue.Enqueue((turnMarker+2, null));
            } else {
                if (players.Find(p => p == combatEntity)) { // player turn
                    turnOrderQueue.Enqueue((0, combatEntity));
                    isPlayerTurn = true;
                    activePlayer = players.Find(p => p == combatEntity);
                    controls.resetState();
                    while (isPlayerTurn) {
                        await Task.Delay(100);
                    }
                } else { // enemy turn
                    turnOrderQueue.Enqueue((0, combatEntity));
                    var enemy = enemies.Find(e => e == combatEntity);
                    await enemy.behavior.doTurn(this, players, enemies);
                }
            }
            // if all enemies are dead, the battle is over
            if (enemies.TrueForAll(e => e.stats.health <= 0)) {
                controls.showVictoryBanner();
                Debug.Log("Loading world map...");
                await goToScene("Scenes/Scene", LoadSceneMode.Single);
                return;
            } else if (players.TrueForAll(p => p.health <= 0)) {
                controls.showDefeatBanner();
                await goToScene("Scenes/GameOverScene", LoadSceneMode.Additive);
                return;
            }
        }
    }

    // Called by EnemyCombatBehavior
    public async Task handleAttack(CombatEntity attackerCE, CombatEntity defenderCE) {
        this.attacker = attackerCE;
        this.defender = defenderCE;
        var attackerGobj = combatEntityToGameObject(attackerCE);
        var defenderGobj = combatEntityToGameObject(defenderCE);
        var origRot = attackerGobj.transform.rotation;
        var origPos = attackerGobj.transform.position;
        var animCtrl = attackerGobj.GetComponentInChildren<Animator>();
        animCtrl.SetTrigger("triggerCharge");
        var movable = attackerGobj.GetComponent<CombatMovable>();
        await movable.moveToPosition(origPos, getAttackPosition(defenderCE));
        animCtrl.ResetTrigger("triggerCharge");
        animCtrl.SetTrigger("triggerMeleeAttack");
        await Task.Delay(350); // humanoid melee attack anim duration
        animCtrl.ResetTrigger("triggerMeleeAttack");
        animCtrl.SetBool("isReturning", true);
        await movable.moveToPosition(getAttackPosition(defenderCE), origPos);
        animCtrl.SetBool("isReturning", false);
        attackerGobj.transform.rotation = origRot;
        this.attacker = null;
        this.defender = null;
    }

    // Called by InteractableCombatEntity
    public async Task handleAttack(HostileCombatEntity defender) {
        await handleAttack(activePlayer, defender);
    }

    public async Task handleCombatEntityClick(GameObject clickedEntity) {
        var controls = Transform.FindObjectOfType<CombatControls>();
        if (controls.isInAttackState()) {
            var hce = (HostileCombatEntity) gameObjectToCombatEntity(clickedEntity);
            await handleAttack(hce);
            endTurn(controls);
        } else if (controls.isInSpellState()) {
            // cast selected spell on target
            Debug.Log("SPELL COMBAT NOT IMPLEMENTED YET");
        }
    }

    public void endTurn(CombatControls controls) {
        if (controls != null) {
            controls.toWaitingState();
        }
        isPlayerTurn = false;
    }

    public void skipPlayerTurn() {
        // give player a defense bonus?
        endTurn(null);
    }

    public async Task calculateHitAndDamage(string data) {
        var attackerGobj = combatEntityToGameObject(attacker);
        var defenderGobj = combatEntityToGameObject(defender);
        var animCtrl = defenderGobj.GetComponentInChildren<Animator>();
        var ace = attacker;
        var dce = defender;
        int hitChance = ace.getHitChance() + dce.getDodgeChange();
        bool hit = Random.Range(1, 101) >= hitChance;
        if (hit) {
            Debug.Log($"{attackerGobj.name} hit {defenderGobj.name}");
            int damage = ace.doDamageRoll();
            var (takenDamage, isKilled) = dce.takeDamage(damage);
            animCtrl.SetTrigger("triggerAttacked");
            await Task.Delay(667); // humanoid attacked anim duration
            if (isKilled) {
                Debug.Log($"{defenderGobj.name} was killed");
                animCtrl.SetTrigger("triggerCollapse");
                animCtrl.ResetTrigger("triggerAttacked");
                removeEntityFromTurnOrder(dce);
            } else {
                animCtrl.ResetTrigger("triggerAttacked");
            }
        } else {
            Debug.Log($"{attackerGobj.name} missed {defenderGobj.name}");
            animCtrl.SetTrigger("triggerMissed");
            await Task.Delay(667); // humanoid missed anim duration
            animCtrl.ResetTrigger("triggerMissed");
        }
    }

    private async Task<List<Player>> instantiatePlayers() {
        var combatCoords = GlobalGameData.players[GlobalGameData.combatInitiatorIdx].coordinates;
        var playerGObj = GameObject.Find("Players").transform;
        var players = new List<Player>();
        for (int i = 0; i < GlobalGameData.players.Count; i++) {
            var playerClone = Instantiate<Player>(playerPrefab, Vector3.zero,
                Quaternion.Euler(0, -90, 0), playerGObj);
            playerClone.transform.position = playerPositions[i];
            playerClone.transform.localScale = new Vector3(2, 2, 2);
            playerClone.name = $"Player {i+1}";
            playerClone.pasteStats(GlobalGameData.players[i]);
            playerClone.combat.isInRange = playerClone.coordinates.DistanceTo(combatCoords) < 2;
            players.Add(playerClone);
        }

        foreach (var player in players) {
            var equipment = player.getEquippedItems();
            foreach (var (slot, stats) in equipment) {
                if (stats.modelName != null && stats.modelName.Length > 0) {
                    var address = $"Assets/Models/Equipment/{stats.modelName}.fbx";
                    Addressables.LoadAssetAsync<GameObject>(address)
                        .Completed += (r) => player.attachItemModel(slot, stats, r.Result);
                }
            }
            if (!player.combat.isInRange) {
                player.combat.turnsToArrival = getTurnsToArrival(player.coordinates, combatCoords);
            }
            var animCtrl = player.GetComponentInChildren<Animator>();
            animCtrl.SetBool("isCombatStarted", true);
        }
        return players;
    }

    private async Task<List<HostileCombatEntity>> instantiateEnemies() {
        int i = 0;
        var enemies = new List<HostileCombatEntity>();
        var prefabs = new Dictionary<string, GameObject>();
        foreach (var enemyStat in GlobalGameData.enemiesInCombat) {
            var key = $"Assets/Prefabs/Enemies/{enemyStat.prefab}.prefab";
            if (!prefabs.ContainsKey(key)) {
                var handle = Addressables.LoadAssetAsync<GameObject>(key);
                await handle.Task;
                prefabs[key] = handle.Result;
            }
            var enemyPosObj = battleArea.transform.Find("Enemies");
            var enemy = Instantiate(prefabs[key], Vector3.zero, Quaternion.Euler(0, 90, 0), enemyPosObj);
            enemy.transform.localPosition = enemyPositions[i];
            enemy.name = $"{enemyStat.name} {i}";
            var animCtrl = enemy.GetComponentInChildren<Animator>();
            animCtrl.SetBool("isCombatStarted", true);
            var hce = enemy.GetComponent<HostileCombatEntity>();
            hce.setStats((EnemyStats) GlobalGameData.enemiesInCombat[i].Clone());
            var ice = enemy.GetComponentInChildren<InteractableCombatEntity>();
            ice.setStats(hce.stats);
            ice.setPopupRef(infoPopup);
            enemies.Add(hce);
            i++;
        }
        return enemies;
    }

    private List<CombatEntity> determineInitiatives(List<Player> players, List<HostileCombatEntity> enemies) {
        // TODO roll for initiative and add modifiers for each entity
        // TODO sort list by initiative
        var entities = new List<CombatEntity>();
        entities.AddRange(players);
        entities.AddRange(enemies);
        return entities;
    }

    private Vector3 getAttackPosition(CombatEntity defender) {
        var player = players.Find(p => p == defender);
        if (player) {
            return player.transform.position + new Vector3(-1, 0, 0);
        } else {
            var enemy = enemies.Find(e => e == defender);
            return enemy.transform.position + new Vector3(1, 0, 0);
        }
    }

    private CombatEntity gameObjectToCombatEntity(GameObject gobj) {
        return enemies.Find(e => e.gameObject == gobj);
    }

    private GameObject combatEntityToGameObject(CombatEntity ce) {
        var player = players.Find(p => p == ce);
        if (player) {
            return player.gameObject;
        } else {
            var enemy = enemies.Find(e => e == ce);
            return enemy.gameObject;
        }
    }

    private void removeEntityFromTurnOrder(CombatEntity entity) {
        var turnOrderArray = turnOrderQueue.ToArray();
        turnOrderQueue.Clear();
        for (int i = 0; i < turnOrderArray.Length; i++) {
            if (turnOrderArray[i].Item2 != entity) {
                turnOrderQueue.Enqueue(turnOrderArray[i]);
            }
        }
        turnOrderViz.init(turnOrderQueue.ToArray());
    }

    private async Task goToScene(string sceneName, LoadSceneMode loadSceneMode) {
        var loadingScreen = GameObject.Find("LoadingScreenCanvas");
        GameObject.DontDestroyOnLoad(loadingScreen);
        var loadingPanel = loadingScreen.transform.GetChild(0).gameObject;
        loadingPanel.SetActive(true);
        AsyncOperation loadingScene = SceneManager.LoadSceneAsync("Scenes/Scene");
        while (!loadingScene.isDone) {
            await Task.Delay(100);
        }
        loadingPanel.SetActive(false);
        GameObject.Destroy(loadingScreen);
    }

    private int getTurnsToArrival(HexCoordinates playerCoords, HexCoordinates combatCoords) {
        return Mathf.FloorToInt(combatCoords.DistanceTo(playerCoords) / 10f);
    }
}
