using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class InteractableCombatEntity : MonoBehaviour {
    private GameObject infoPopupRoot;
    private Coroutine popupCoroutine;
    private EnemyStats stats;

    void Start() {}

    public void setPopupRef(GameObject infoPopupRoot) {
        this.infoPopupRoot = infoPopupRoot;
    }

    public void OnMouseEnter() {
        if (popupCoroutine == null) {
            popupCoroutine = StartCoroutine(showInfoPopup(stats.description));
        }
    }

    public void OnMouseExit() {
        if (popupCoroutine != null) {
            closeInfoPopup();
            StopCoroutine(popupCoroutine);
            popupCoroutine = null;
        }
    }

    public void setStats(EnemyStats stats) {
        this.stats = stats;
    }

    public async void OnMouseUpAsButton() {
        // does different things depending on CombatControls state
        var turnManager = Transform.FindObjectOfType<CombatTurnManager>();
        await turnManager.handleCombatEntityClick(gameObject);
    }

    IEnumerator showInfoPopup(string text) {
        yield return new WaitForSeconds(0.6f);
        infoPopupRoot.GetComponentInChildren<EnemyInfoPopupHandler>(true).init(text);
        infoPopupRoot.gameObject.SetActive(true);
    }

    private void closeInfoPopup() {
        infoPopupRoot.gameObject.SetActive(false);
    }
}
