using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombatControls : MonoBehaviour {
    public GameObject actionsPanel;
    public GameObject bannerPanel;

    private enum State {
        DEFAULT_MODE,
        ATTACK_MODE,
        SPELL_MODE,
        INVENTORY_MODE,
        RETREAT_MODE,
        WAIT_MODE, // wait for next turn
    }

    private State state;

    void Start() {
        state = State.DEFAULT_MODE;
    }

    void Update() {
        if (state != State.DEFAULT_MODE) {
            if (Input.GetKeyUp(KeyCode.Escape)) {
                state = State.DEFAULT_MODE;
                showActions();
            }
        } else {
            if (Input.GetKeyUp(KeyCode.A)) {
                onAttackClick();
            } else if (Input.GetKeyUp(KeyCode.S)) {
                onSpellbookClick();
            } else if (Input.GetKeyUp(KeyCode.D)) {
                onInventoryClick();
            } else if (Input.GetKeyUp(KeyCode.F)) {
                onRetreatClick();
            } else if (Input.GetKeyUp(KeyCode.G)) {
                onSkipTurnClick();
            }
        }
    }

    public void onAttackClick() { // triggered by mouse or keyboard
        state = State.ATTACK_MODE;
        hideActions();
        showModeIndicator(state);
    }

    public void onSpellbookClick() { // triggered by mouse or keyboard
        state = State.SPELL_MODE;
        hideActions();
        showModeIndicator(state);
    }

    public void onInventoryClick() { // triggered by mouse or keyboard
        state = State.INVENTORY_MODE;
        hideActions();
        showModeIndicator(state);
    }

    public void onRetreatClick() { // triggered by mouse or keyboard
        state = State.RETREAT_MODE;
        hideActions();
        showModeIndicator(state);
    }

    public void onSkipTurnClick() { // triggered by mouse or keyboard
        var turnManager = Transform.FindObjectOfType<CombatTurnManager>();
        turnManager.skipPlayerTurn();
        toWaitingState();
    }

    public void resetState() {
        state = State.DEFAULT_MODE;
        showActions();
    }

    public void toWaitingState() {
        state = State.WAIT_MODE;
        hideActions();
    }

    public bool isInAttackState() {
        return state == State.ATTACK_MODE;
    }

    public bool isInSpellState() {
        return state == State.SPELL_MODE;
    }

    private void hideActions() {
        var actions = actionsPanel.GetComponentsInChildren<Transform>();
        foreach (var action in actions) {
            action.gameObject.SetActive(false);
        }
    }

    private void showActions() {
        var actions = actionsPanel.GetComponentsInChildren<Transform>(true);
        foreach (var action in actions) {
            action.gameObject.SetActive(true);
        }
    }

    private void showModeIndicator(State state) {
        Debug.Log($"Mode is {state}");
    }

    public void showVictoryBanner() {
        bannerPanel.SetActive(true);
        bannerPanel.transform.GetChild(0).gameObject.SetActive(true);
    }

    public void showDefeatBanner() {
        bannerPanel.SetActive(true);
        bannerPanel.transform.GetChild(1).gameObject.SetActive(true);
    }
}
