using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnOrderVisualizer : MonoBehaviour {
    public GameObject turnIconPrefab;
    public GameObject playerIconPrefab;
    public GameObject enemyIconPrefab;

    void Start() {}

    void Update() {}

    public void init((int, CombatEntity)[] turnOrder) {
        var list = new ArrayList();
        foreach (Transform item in transform) {
            list.Add(item.gameObject);
        }
        foreach (GameObject item in list) {
            Destroy(item);
        }

        foreach (var (turnMarker,entity) in turnOrder) {
            if (turnMarker > 0) {
                var turnIcon = Instantiate(turnIconPrefab, Vector3.zero, Quaternion.identity, transform);
                turnIcon.GetComponentInChildren<TMPro.TMP_Text>().text = $"{turnMarker}";
            } else if (entity.isPlayer()) {
                Instantiate(playerIconPrefab, Vector3.zero, Quaternion.identity, transform);
                // TODO add popup with name
            } else {
                Instantiate(enemyIconPrefab, Vector3.zero, Quaternion.identity, transform);
                // TODO add popup with name
            }
        }
    }
}