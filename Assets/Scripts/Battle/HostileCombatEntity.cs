using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HostileCombatEntity : MonoBehaviour, CombatEntity {
    public EnemyCombatBehavior behavior;
    internal EnemyStats stats;

    void Start() {
        behavior = new EnemyCombatBehavior(this);
    }

    void Update() {}

    public void setStats(EnemyStats stats) {
        this.stats = stats;
    }

    // Reduce entity health by damage subtracted by damage reduction
    public (int, bool) takeDamage(int damage) {
        int effectiveDamage = Mathf.Abs(damage - stats.damageReduction);
        stats.health -= effectiveDamage;
        Debug.Log($"{this.gameObject.name} took {effectiveDamage} points of damage (HP: {stats.health})");
        if (stats.health <= 0) {
            stats.health = 0;
            return (effectiveDamage, true);
        }
        return (effectiveDamage, false);
    }

    // Get damage dealt by currently equipped weapon
    public int doDamageRoll() {
        return 1; // TODO use weapon dmg range and modifiers
    }

    // Get chance to hit. The lower the easier to hit.
    public int getHitChance() {
        // TODO use stats/eq to modify
        // return 25; // 75% chance to hit
        return 1;
    }

    // Get change to dodge. The higher the easier to dodge.
    public int getDodgeChange() {
        return 0; // TODO use stats/eq to modify
    }

    public bool isPlayer() {
        return false;
    }
}
