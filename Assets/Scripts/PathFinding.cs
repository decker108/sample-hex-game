using System;
using UnityEngine;
using Priority_Queue;
using System.Collections.Generic;

// All credit for implementation goes to Wikipedia.
// https://en.wikipedia.org/wiki/A*_search_algorithm
public class PathFinding {
    public static HexCell[] previousPath = Array.Empty<HexCell>();
    private static Dictionary<(HexCoordinates,HexCoordinates), HexCell[]> pathCache = 
        new Dictionary<(HexCoordinates, HexCoordinates), HexCell[]>();

    public static HexCell[] FindPath(HexCell start, HexCell goal, HexMapSearchable hexGrid, bool useCache = true) {
        if (useCache && pathCache.ContainsKey((start.coordinates, goal.coordinates)) 
            || pathCache.ContainsKey((goal.coordinates, start.coordinates))) {
            return pathCache[(start.coordinates, goal.coordinates)];
        }
        var path = aStar(start, goal, (HexCell start) => start.DistanceTo(goal), hexGrid);
        if (useCache) {
            pathCache[(start.coordinates, goal.coordinates)] = path;
        }
        return path;
    }

    public static void resetCache() {
        pathCache.Clear();
    }

    public static List<HexCell> getSubPathLimitedByMovementPoints(List<HexCell> path) {
        // TODO implement this
        return null;
    }

    private static HexCell[] reconstructPath(Dictionary<HexCell, HexCell> cameFrom, HexCell current) {
        var total_path = new List<HexCell>();
        total_path.Add(current);
        while (cameFrom.ContainsKey(current)) {
            current = cameFrom[current];
            total_path.Add(current); // Since C# lists lack a Prepend method, we append and reverse.
        }
        total_path.Reverse();
        return total_path.ToArray();
    }

    private static HexCell[] aStar(HexCell start, HexCell goal, Func<HexCell, float> h, HexMapSearchable hexGrid) {
        // The set of discovered nodes that may need to be (re-)expanded.
        // Initially, only the start node is known.
        // This is usually implemented as a min-heap or priority queue rather than a hash-set.
        var openSet = new SimplePriorityQueue<HexCell>(); // TODO replace with fast prio queue if slow
        openSet.Enqueue(start, int.MaxValue);

        // For node n, cameFrom[n] is the node immediately preceding it on the cheapest path from start
        // to n currently known.
        var cameFrom = new Dictionary<HexCell, HexCell>();

        // For node n, gScore[n] is the cost of the cheapest path from start to n currently known.
        var gScore = new Dictionary<HexCell, float>(); // map with default value of Infinity
        gScore[start] = 0f;

        // For node n, fScore[n] := gScore[n] + h(n). fScore[n] represents our current best guess as to
        // how short a path from start to finish can be if it goes through n.
        var fScore = new Dictionary<HexCell, float>(); // map with default value of Infinity
        fScore[start] = h(start);

        HexCell current;
        while (openSet.Count > 0) {
            // This operation can occur in O(1) time if openSet is a min-heap or a priority queue
            current = openSet.Dequeue(); // the node in openSet having the lowest fScore[] value
            if (current == goal) {
                return reconstructPath(cameFrom, current);
            }

            var neighbors = hexGrid.GetHexesForCoordinates(HexCoordinates.GetNeighbors(current.coordinates))
                .FindAll((HexCell hc) => filterTerrain(hc));
            foreach (var neighbor in neighbors) {
                // d(current,neighbor) is the weight of the edge from current to neighbor
                // tentative_gScore is the distance from start to the neighbor through current
                float tentative_gScore = gScore[current] + current.DistanceTo(neighbor);
                if (tentative_gScore < getNeighborGScoreOrDefault(gScore, neighbor)) {
                    // This path to neighbor is better than any previous one. Record it!
                    cameFrom[neighbor] = current;
                    gScore[neighbor] = tentative_gScore;
                    fScore[neighbor] = gScore[neighbor] + start.DistanceTo(neighbor);
                    if (!openSet.Contains(neighbor)) {
                        openSet.Enqueue(neighbor, 1); // TODO this priority might be useful for terrain with movement penalties
                    }
                }
            }
        }

        // Use this to return a partial path
        // return reconstructPath(current);

        // Open set is empty but goal was never reached
        return Array.Empty<HexCell>();
    }

    private static bool filterTerrain(HexCell cell) {
        // Currently, we only disallow crossing mountain hexes
        return cell.terrainType != TerrainType.Name.MOUNTAIN;
    }

    private static float getNeighborGScoreOrDefault(Dictionary<HexCell, float> gScore, HexCell neighbor) {
        float value;
        if (gScore.TryGetValue(neighbor, out value)) {
            return value;
        }
        return float.MaxValue;
    }
}
