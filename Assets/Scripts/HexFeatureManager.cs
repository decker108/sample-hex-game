using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class HexFeatureManager {
    private static int enemyIdx = 0;
    private static int playerIdx = 0;

    public static void create(int x, int z, HexMapSearchable hms, Func<Type, object> instantiatorFn, Action<object> listAdderFn) {
        string key = $"{x},{z}";
        var hf = GlobalGameData.terrainMapping.hexFeatures[key];
        if (hf.ContainsKey("playerStartingPoint")) {
            var player = (Player) instantiatorFn(typeof(Player));
            player.GetComponent<MeshRenderer>().material.color = new Color(0, 1, 0, 0);
            player.name = $"Player {++playerIdx}";
            player.coordinates = new HexCoordinates(x, z);
            var startEq = GlobalGameData.startingEquipment
                .ConvertAll<(ItemStats.BodySlot, ItemStats)>((x) => (x.Item1, (ItemStats) x.Item2.Clone()));
            player.setStartingEquipment(startEq);
            
            listAdderFn(player);
        } else if (hf.ContainsKey("npcStartingPoint")) {
            var npc = (Npc) instantiatorFn(typeof(Npc));
            npc.GetComponent<MeshRenderer>().material.color = new Color(0, 0, 1, 0);
            npc.coordinates = new HexCoordinates(x,z);
            NpcDialogue dialog = npc.GetComponent<NpcDialogue>();
            listAdderFn(npc);
        } else if (hf.ContainsKey("enemyStartingPoint")) {
            var enemy = (Enemy) instantiatorFn(typeof(Enemy));
            enemy.name = $"Enemy {++enemyIdx}";
            if (hf.ContainsKey("behavior")) {
                Type behaviorType = Type.GetType(hf["behavior"]);
                var behavior = Activator.CreateInstance(behaviorType, new object[]{enemy, hms});
                enemy.setBehavior((EnemyBehavior) behavior);
            } else {
                enemy.setBehavior(new EnemyBehavior(enemy, hms));
            }
            enemy.GetComponent<MeshRenderer>().material.color = new Color(1, 0, 0, 0);
            enemy.coordinates = new HexCoordinates(x,z);
            if (hf.ContainsKey("patrolRoute") && hf["patrolRoute"].Length > 0) {
                var patrolRoute = new List<string>(hf["patrolRoute"].Split(';'))
                    .ConvertAll<HexCoordinates>(str => new HexCoordinates(int.Parse(str.Split(',')[0]), int.Parse(str.Split(',')[1])));
                enemy.setPatrolRoute(patrolRoute);
            }
            listAdderFn(enemy);
        }
    }
}
