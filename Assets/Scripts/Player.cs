using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading.Tasks;
using System;

public class Player : MonoBehaviour, CombatEntity {

    public HexCoordinates coordinates;
    public Spell activeSpell;

    public int health = 10;
    public int mana = 10;

    private float maxMovementPoints = 5;
    public float movementPoints;

    private List<ItemStats> inventory = new List<ItemStats>();
    private List<(ItemStats.BodySlot, ItemStats)> equippedItems = new List<(ItemStats.BodySlot, ItemStats)>();

    public CombatLogic combat = new CombatLogic();
    public GameObject leftHand, rightHand;

    void Start() {
        var movable = GetComponent<Movable>();
        movable.setCallback((hex) => {
            coordinates = hex.coordinates;
            movementPoints--;
        });
    }

    void Update() {
    }

    public void resetMovementPoints() {
        movementPoints = maxMovementPoints;
    }

    public List<ItemStats> getInventory() {
        return inventory;
    }

    public List<(ItemStats.BodySlot, ItemStats)> getEquippedItems() {
        return equippedItems;
    }

    public void equipItem(ItemStats item, ItemStats.BodySlot bodySlot) {
        equippedItems.Add((bodySlot, item));
    }

    public void unequipItem(ItemStats item, ItemStats.BodySlot bodySlot) {
        equippedItems.Remove((bodySlot, item));
    }

    public async Task<bool> move(HexCell[] path, Func<HexCoordinates, bool> cancelCallback) {
        return await GetComponent<Movable>().MoveAlongPath(path, true, cancelCallback);
    }

    public void setStartingEquipment(List<(ItemStats.BodySlot, ItemStats)> items) {
        foreach (var (slot,item) in items) {
            if (slot == ItemStats.BodySlot.NONE) {
                inventory.Add(item);
            } else {
                equippedItems.Add((slot, item));
            }
        }
    }

    public void attachItemModel(ItemStats.BodySlot slot, ItemStats item, GameObject model) {
        if (slot == ItemStats.BodySlot.RIGHT_ARM) {
            var hand = rightHand;
            GameObject gobj = Instantiate(model, hand.transform.position, 
                new Quaternion(0, 0, 0, 0), hand.transform);
            gobj.transform.localScale = new Vector3(0.3f,0.3f,0.3f); // TODO get from ItemStats
            gobj.transform.localRotation = Quaternion.Euler(0, 0, 0); // TODO get from ItemStats
            gobj.transform.localPosition = new Vector3(0,-0.0015f,0.005f); // TODO get from ItemStats
        } else if (slot == ItemStats.BodySlot.LEFT_ARM) {
            var hand = leftHand;
            GameObject gobj = Instantiate(model, hand.transform.position, 
                new Quaternion(0, 0, 0, 0), hand.transform);
            gobj.transform.localScale = new Vector3(0.005f,0.005f,0.005f); // TODO get from ItemStats
            gobj.transform.localRotation = Quaternion.Euler(90, 270, 0); // TODO get from ItemStats
            gobj.transform.localPosition = new Vector3(-0.0015f,0,0); // TODO get from ItemStats
        }
    }

    public void pasteStats(Player original) {
        this.inventory = original.inventory;
        this.health = original.health;
        this.mana = original.mana;
        this.equippedItems = original.equippedItems;
        this.combat.damageReduction = original.combat.damageReduction;
        this.combat.hitModifier = original.combat.hitModifier;
        this.combat.dodgeModifier = original.combat.dodgeModifier;
        this.coordinates = original.coordinates;
    }

    public class CombatLogic {
        public bool isInRange = true; // if not in range, teleport to combat
        public int turnsToArrival = 0;
        public int damageReduction = 0;
        public int hitModifier = 0;
        public int dodgeModifier = 0;
        public float initiative = 0;
    }

    // Reduce player health by damage subtracted by damage reduction
    public (int, bool) takeDamage(int damage) {
        int effectiveDamage = Mathf.Abs(damage - combat.damageReduction);
        health -= effectiveDamage;
        Debug.Log($"{this.gameObject.name} took {effectiveDamage} points of damage");
        if (health <= 0) {
            health = 0;
            return (effectiveDamage, true);
        }
        return (effectiveDamage, false);
    }

    // Get damage dealt by currently equipped weapon
    public int doDamageRoll() {
        return 50; // TODO use weapon dmg range and modifiers
    }

    // Get chance to hit. The lower the easier to hit.
    public int getHitChance() {
        // TODO use stats/eq to modify
        return 25;
    }

    // Get change to dodge. The higher the easier to dodge.
    public int getDodgeChange() {
        return 0; // TODO use stats/eq to modify
    }

    public bool isPlayer() {
        return true;
    }
}
