using System.Collections.Generic;

public interface HexMapSearchable {
    List<HexCell> GetHexesForCoordinates(List<HexCoordinates> coords);

	HexCell GetHexForCoordinate(HexCoordinates coord);

    bool isNpcInTile(HexCoordinates coordinates);

	bool isPlayerInTile(HexCoordinates coordinates);

    bool isEnemyInTile(HexCoordinates coordinates);
}