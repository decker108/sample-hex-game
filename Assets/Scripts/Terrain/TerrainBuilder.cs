using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainBuilder : MonoBehaviour {
	public TerrainType terrainPrefab;

    public GameObject mountainPrefab;
    public GameObject treePrefab;
    public GameObject grassPrefab;
    public GameObject lakePrefab;
    public GameObject swampPrefab;
    public GameObject morainePrefab;
    public GameObject dunePrefab;
    public GameObject tundraPrefab;
    public GameObject icePrefab;

    public Material grassMaterial;
    public Material duneMaterial;
    public Material tundraMaterial;
    public Material dirtMaterial;
    public Material snowMaterial;
    public Material gravelMaterial;
    public Material burntGrassMaterial;
    public Material lavaMaterial;
}
