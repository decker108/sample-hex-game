using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainType : MonoBehaviour {

    public enum Name: ushort {
        GRASSLAND = 1,
        FOREST = 2,
        LAKE = 3,
        MOUNTAIN = 4,
        SWAMP = 5,
        MORAINE = 6, // e.g. broken rocks
        DUNE = 7,
        TUNDRA = 8,
        ICE = 9,
        BURNED_GRASSLAND = 10,
        SNOW = 11,
        LAVA = 12,
    }

    public enum Status: ushort {
        NORMAL,
        FROZEN,
        BURNING,
        CURSED,
        BLESSED,
    }

    private TerrainBuilder terrainBuilder;

    void Start() {
        terrainBuilder = GameObject.Find("Hex Grid").GetComponentInChildren<TerrainBuilder>();
    }

    public void renderTerrain(Name terrain, TerrainBuilder builder, HexCell cell, Status status = Status.NORMAL) {
        cell.terrainType = terrain;

        var renderer = cell.gameObject.GetComponent<Renderer>();
        if (terrain == Name.MOUNTAIN) {
            cell.SetNonRandomColor(0.2f, 0.4f, 0.2f);
            var mountain = Instantiate(builder.mountainPrefab,
                transform.position, transform.rotation, transform);
            // mountain.setClimate(???)
        } else if (terrain == Name.FOREST) {
            renderer.material = builder.dirtMaterial;
            for (int i = 0; i < 12; i++) {
                int a = Random.Range(0, 359); // angle from center
                float l = Random.Range(0f, 5.0f * Mathf.Sqrt(3.0f)); // length from center
                Vector3 pos = transform.position + new Vector3(l * Mathf.Cos(a), 0, l * Mathf.Sin(a));
                var tree = Instantiate(builder.treePrefab, pos,
                    Quaternion.Euler(90, 0, 0), transform);
                // tree.setClimate(???)
            }
            // set climate for underlying hex too
        } else if (terrain == Name.MORAINE) {
            renderer.material = builder.gravelMaterial;
            var moraine = Instantiate(builder.morainePrefab, transform.position,
                Quaternion.Euler(0, Random.Range(1, 359), 0), transform);
            // moraine.setClimate(???)
        } else if (terrain == Name.LAKE) {
            if (status == Status.FROZEN) {
                cell.terrainStatus = Status.FROZEN;
                // TODO replace snow with frozen lake material
                renderer.material = builder.snowMaterial;
            } else if ((status == Status.BURNING && cell.terrainStatus == Status.FROZEN) || status == Status.NORMAL) {
                cell.terrainStatus = Status.NORMAL;
                cell.SetNonRandomColor(0.4f, 0.4f, 1f);
            }
            // TODO add water shader here
        } else if (terrain == Name.SWAMP) {
            Instantiate(builder.swampPrefab, transform.position,
                Quaternion.Euler(0, Random.Range(1, 359), 0), transform);
        } else if (terrain == Name.DUNE) {
            renderer.material = builder.duneMaterial;
            // randomly spawn sandy rocks
        } else if (terrain == Name.TUNDRA) {
            renderer.material = builder.tundraMaterial;
            // randomly spawn icy rocks
        } else if (terrain == Name.ICE) {
            cell.SetNonRandomColor(255f, 255f, 255f);
            Instantiate(builder.icePrefab, transform.position + new Vector3(0f, 0f, 0f),
                Quaternion.Euler(0, Random.Range(0, 359), 0), transform);
        } else if (terrain == Name.BURNED_GRASSLAND) {
            renderer.material = builder.burntGrassMaterial;
            // randomly spawn smoke pillars
        } else if (terrain == Name.SNOW) {
            renderer.material = builder.snowMaterial;
            // randomly spawn icy rocks
        } else if (terrain == Name.LAVA) {
            renderer.material = builder.lavaMaterial;
            // play looping lava bubbles VFX
        } else { // default to grassland
            Instantiate(builder.grassPrefab, transform.position,
                Quaternion.Euler(0, Random.Range(1, 359), 0), transform);
        }
    }

    public void freezeTerrain(HexCell cell) {
        switch (cell.terrainType) {
            case Name.GRASSLAND: {
                transformTerrain(cell, Name.SNOW);
                PathFinding.resetCache();
                break;
            }
            case Name.LAKE: {
                transformTerrain(cell, Name.LAKE, Status.FROZEN);
                PathFinding.resetCache();
                break;
            }
            default: break;
        }
    }

    public void burnTerrain(HexCell cell) {
        switch (cell.terrainType) {
            case Name.GRASSLAND: // turns into burnt grass
            case Name.TUNDRA: // turns into burnt grass
            case Name.FOREST: { // turns into burnt grass
                transformTerrain(cell, Name.BURNED_GRASSLAND);
                PathFinding.resetCache();
                break;
            }
            case Name.SWAMP: { // turns into lake
                transformTerrain(cell, Name.LAKE);
                PathFinding.resetCache();
                break;
            }
            case Name.SNOW: // turns into tundra
            case Name.ICE: { // turns into tundra
                transformTerrain(cell, Name.LAKE);
                PathFinding.resetCache();
                break;
            }
            default: break;
        }
    }

    public void floodTerrain(HexCell cell) {
        switch (cell.terrainType) {
            case Name.FOREST: {
                transformTerrain(cell, Name.SWAMP);
                PathFinding.resetCache();
                break;
            }
            case Name.TUNDRA: {
                transformTerrain(cell, Name.SNOW);
                PathFinding.resetCache();
                break;
            }
            default: break;
        }
    }

    public void shockTerrain(HexCell cell) {
        switch (cell.terrainType) {
            case Name.FOREST: { // turns into burnt grass
                transformTerrain(cell, Name.FOREST, Status.BURNING);
                PathFinding.resetCache();
                break;
            }
            case Name.LAKE: {
                // damage players and enemies in lake
                break;
            }
            default: break;
        }
    }

    private void transformTerrain(HexCell cell, Name newTerrain, Status status = Status.NORMAL) {
        var oldTerrain = cell.GetComponentInChildren<TerrainType>().gameObject;
        Destroy(oldTerrain);
        var terrainType = Instantiate<TerrainType>(
			terrainBuilder.terrainPrefab, cell.transform.position + new Vector3(0, 1, 0), 
			Quaternion.Euler(0, 0, 0), cell.transform);
        renderTerrain(newTerrain, terrainBuilder, cell, status);
    }

    public static string getHumanReadableName(TerrainType.Name name) {
        return System.Enum.GetName(typeof(TerrainType.Name), name);
    }

    public static string getHumanReadableName(TerrainType.Status name) {
        return System.Enum.GetName(typeof(TerrainType.Status), name);
    }
}
