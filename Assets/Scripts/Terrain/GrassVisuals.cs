using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrassVisuals : MonoBehaviour {
    public Material grassMaterial;
    public GameObject flowerPrefab;
    public GameObject flowerPrefab2;
    public GameObject flowerPrefab3;

    void Start() {
        var renderer = transform.parent.parent.GetComponent<MeshRenderer>();
        renderer.material = grassMaterial;

        var flowerDecals = new GameObject[]{flowerPrefab, flowerPrefab2, flowerPrefab3};
        var noOfFlowers = Random.Range(0, 3);
        for (int i = 0; i < noOfFlowers; i++) {
            int a = Random.Range(0, 359); // angle from center
            float l = Random.Range(0f, 4.0f * Mathf.Sqrt(3.0f)); // length from center
            Vector3 pos = transform.position + new Vector3(l * Mathf.Cos(a), 0.2f, l * Mathf.Sin(a));
            var gobj = Instantiate(flowerDecals[Random.Range(0, flowerDecals.Length)], pos,
                Quaternion.Euler(0, Random.Range(1, 359), 0), transform);
        }
        // TODO spawn mossy rocks
        // TODO spawn grass tufts
    }

    void Update() {
        
    }
}
