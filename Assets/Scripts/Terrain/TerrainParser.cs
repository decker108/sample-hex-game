using System;
using System.IO;
using UnityEngine;
using SimpleJSON;
using System.Collections.Generic;

public class TerrainParser {
    public static TerrainMapping ParseFile(string filePath) {
        string json = File.ReadAllText(filePath);
        var blob = JSON.Parse(json);

        var output = new TerrainMapping();
        output.map = new int[HexCoordinates.maxZ][];
        // Matrixes are stored with the origo in the top-left, but our map has the origo in the bottom left.
        // So we need to read from top to bottom, but write from bottom to top.
        for (int i = HexCoordinates.maxZ-1, h = 0; i >= 0; i--, h++) {
            output.map[i] = new int[HexCoordinates.maxZ];
            for (int j = 0; j < HexCoordinates.maxZ; j++) {
                output.map[i][j] = blob["map"][h][j].AsInt;
            }
        }

        var feats = blob["hexFeatures"];
        output.hexFeatures = new Dictionary<string, Dictionary<string, string>>(feats.Count);
        foreach (var key in feats.Keys) {
            var subDict = new Dictionary<string, string>();
            foreach (var subKey in feats[key].Keys) {
                subDict.Add(subKey, feats[key][subKey]);
            }
            output.hexFeatures.Add(key, subDict);
        }
        return output;
    }

    public class TerrainMapping {
        public int[][] map;
        public Dictionary<string, Dictionary<string, string>> hexFeatures;
    }
}
