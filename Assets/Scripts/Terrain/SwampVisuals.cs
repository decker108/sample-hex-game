using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwampVisuals : MonoBehaviour {
    public Material material1;
    public Material material2;
    public Material material3;

    void Start() {
        var renderer = transform.parent.parent.GetComponent<MeshRenderer>();
        var materials = new Material[]{material1, material2, material3};
        renderer.material = materials[Random.Range(0, 2)];
        // TODO randomly spawn trees and reeds
    }

    void Update() {
        
    }
}
