using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading.Tasks;

/** Enemy AI behavior superclass **/
public class EnemyBehavior {
    protected enum State {
        IDLING,
        PATROLLING,
        ATTACKING,
        RETREATING,
        SEARCHING,
    }
    protected State previousState;
    protected State state;
    protected Enemy enemy;
    protected static HexMapSearchable hexGrid;
    protected int searchCountdown = 0;

    public EnemyBehavior(Enemy enemy, HexMapSearchable hexGrid) {
        state = previousState = State.IDLING;
        this.enemy = enemy;
        EnemyBehavior.hexGrid = hexGrid;
    }

    // This methods runs repeatedly until isDone is true
    public async Task doTurn() { // Sense, Think, Act
        if (state == State.IDLING) {
            await handleIdling();
        } else if (state == State.PATROLLING) {
            await handlePatrolling();
        } else if (state == State.ATTACKING) {
            await handleAttacking();
        } else if (state == State.RETREATING) {
            await handleRetreating();
        } else if (state == State.SEARCHING) {
            await handleSearching();
        }

        return;
    }

    protected async virtual Task handleIdling() {
        transitionState(State.IDLING);
        if (canSeeHostiles(enemy.visionRange)) {
            await handleAttacking();
        } else if (enemy.getPatrolWaypoint() != null) {
            await handlePatrolling();
        }
    }

    protected async virtual Task handlePatrolling() {
        transitionState(State.PATROLLING);
        if (canSeeHostiles(enemy.visionRange)) { // check for hostiles. if found, transition to ATTACKING
            await handleAttacking();
            return;
        }
        var wp = enemy.getPatrolWaypoint();
        // Debug.Log($"Got patrol wp: {wp}");
        HexCell[] path = PathFinding.FindPath(
            hexGrid.GetHexForCoordinate(enemy.coordinates), 
            hexGrid.GetHexForCoordinate(wp), 
            hexGrid, useCache: false);
        // TODO get a path limited by movement points
        if (path == null) {
            // if can't move along patrol route, end turn
            return;
        }
        // Debug.Log($"Got patrol path with length: {path.Length}");
        List<HexCell> route = new List<HexCell>(path).GetRange(0, path.Length);
        // Debug.Log($"Route to target with length: {route.Count}");
        var foundHostile = await enemy.move(
                route.GetRange(0, Mathf.Min((int) enemy.movementPoints, route.Count)).ToArray(),
                coords => canSeeHostiles(enemy.visionRange));
        if (foundHostile) {
            await handleAttacking();
        }
        // if no more moves, end turn
    }

    protected async virtual Task handleAttacking() {
        transitionState(State.ATTACKING);
        if (isHostileAdjacent()) { // if hostile is adjacent, enter combat
            enterCombat();
        } else if (canSeeHostiles(enemy.visionRange)) { // if hostile is out of range:
            // move one hex at a time until adjacent or until out of moves
            // if moves above zero, move closer
            HexCell[] path = PathFinding.FindPath(
                hexGrid.GetHexForCoordinate(enemy.coordinates), 
                hexGrid.GetHexForCoordinate(getClosestPlayerPos(enemy.visionRange)), 
                hexGrid, useCache: false);
            List<HexCell> routeToPlayer = new List<HexCell>(path).GetRange(0, path.Length - 1);
            // Debug.Log($"Route to player: from {routeToPlayer[0]} to {routeToPlayer[routeToPlayer.Count-1]}");
            var inCombatRange = await enemy.move(routeToPlayer.ToArray(), coords => isHostileAdjacent());
            if (inCombatRange) {
                enterCombat();
            }
        } else { // lost sight of enemy, return to patrol
            // TODO implement this
            Debug.Log("(NOT IMPLEMENTED YET) Returning to patrol route");
        }
    }

    protected async virtual Task handleRetreating() {
        transitionState(State.RETREATING);
        // if hostile is in range, move away
        // TODO get path in opposite direction of player relative to this enemy
            // if home base exists, move towards it
        Debug.Log("(NOT IMPLEMENTED YET) Retreating");
        await Task.Run(() => {Debug.Log("Enemy retreating");});
    }

    protected async virtual Task handleSearching() {
        transitionState(State.SEARCHING);
        if (previousState != State.SEARCHING) {
            searchCountdown = 2;
        } else {
            searchCountdown--;
            if (searchCountdown == 0) { // when countdown is zero, return to patrol
                await handlePatrolling();
                return;
            }
        }
        // hold position X turns while searching for hostiles
        if (canSeeHostiles(enemy.visionRange)) {
            await handleAttacking();
        }
    }

    protected void transitionState(State newState) {
        Debug.Log($"Transitioning from {state} to {newState}");
        previousState = state;
        state = newState;
    }

    protected bool canSeeHostiles(int range) {
        var hexes = HexCoordinates.getConcentricHexes(enemy.coordinates, range);
        foreach (var hex in hexes) {
            if (hexGrid.isPlayerInTile(hex)) {
                // Debug.Log($"{enemy.name} located player inside range {range}");
                return true;
            }
        }
        return false;
    }

    protected bool isHostileAdjacent() {
        // 2 means hex enemy is in and directly surrounding hexes
        return canSeeHostiles(2);
    }

    protected void enterCombat() {
        Debug.Log($"{enemy.name} entering combat with player");
    }

    protected HexCoordinates getClosestPlayerPos(int range) {
        var positions = new List<HexCoordinates>();
        var hexes = HexCoordinates.getConcentricHexes(enemy.coordinates, range);
        foreach (var hex in hexes) {
            if (hexGrid.isPlayerInTile(hex)) {
                // Debug.Log($"{enemy.name} located player");
                positions.Add(hex);
            }
        }
        // hexes.RemoveWhere(hex => !hexGrid.isPlayerInTile(hex));
        // new List<HexCoordinates>(hexes);
        return HexCoordinates.getClosestPosition(enemy.coordinates, positions);
    }
}
