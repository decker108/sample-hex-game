using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading.Tasks;

/** Enemy AI behavior superclass **/
public class ImmobileBehavior : EnemyBehavior {
    public ImmobileBehavior(Enemy enemy, HexMapSearchable hexGrid) : base(enemy, hexGrid) {
        state = previousState = State.IDLING;
        this.enemy = enemy;
        ImmobileBehavior.hexGrid = hexGrid;
    }

    protected async override Task handleIdling() {
        Debug.Log("ImmobileBehavior idling");
        transitionState(State.IDLING);
        await Task.Run(() => {});
    }

    protected async override Task handlePatrolling() {
        Debug.Log("ImmobileBehavior patrolling");
        await Task.Run(() => {});
    }

    protected async override Task handleAttacking() {
        Debug.Log("ImmobileBehavior attacking");
        await Task.Run(() => {});
    }

    protected async override Task handleRetreating() {
        Debug.Log("ImmobileBehavior retreating");
        await Task.Run(() => {});
    }

    protected async override Task handleSearching() {
        Debug.Log("ImmobileBehavior searching");
        await Task.Run(() => {});
    }
}
