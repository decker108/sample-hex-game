using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading.Tasks;

/** Enemy AI behavior subclass for ranged spellcasters **/
public class SpellcasterBehavior : EnemyBehavior {

    public SpellcasterBehavior(Enemy enemy, HexMapSearchable hexGrid) : base(enemy, hexGrid) {
        state = State.IDLING;
    }

    protected async override Task handleAttacking() {
        state = State.ATTACKING;
        // cycle through spells, see if player is in range
        // when out of mana, retreat
        // TODO implement this
        await Task.Run(() => {Debug.Log("Spellcaster attacking");});
    }
}