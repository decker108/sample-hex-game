using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading.Tasks;

/** Enemy AI behavior subclass for neutral critters **/
public class CritterBehavior : EnemyBehavior {
    public CritterBehavior(Enemy enemy, HexMapSearchable hexGrid) : base(enemy, hexGrid) {
        state = State.IDLING;
    }

    protected async override Task handleIdling() {
        state = State.IDLING;
        // move around randomly until an enemy or player is seen, then retreat
        // TODO implement this
        await Task.Run(() => {Debug.Log("Critter idling");});
    }

    protected async override Task handleRetreating() {
        state = State.IDLING;
        // move around randomly until an enemy or player is seen, then retreat
        // TODO implement this
        await Task.Run(() => {Debug.Log("Critter retreating");});
    }
}
