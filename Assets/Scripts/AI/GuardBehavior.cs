using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading.Tasks;

/** Enemy guarding AI behavior superclass **/
public class GuardBehavior : EnemyBehavior {

    public GuardBehavior(Enemy enemy, HexMapSearchable hexGrid) : base(enemy, hexGrid) {
        state = previousState = State.IDLING;
        this.enemy = enemy;
        GuardBehavior.hexGrid = hexGrid;
    }

    // TODO should stay in start pos until hostile is spotted, then chase, attack and return.
}
