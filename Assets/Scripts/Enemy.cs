using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Threading.Tasks;

public class Enemy : MonoBehaviour {
    public HexCoordinates coordinates;
    public int visionRange = 2;
    public int health = 10;
    public int mana = 10;
    public float movementPoints;
    public bool isHostile = true;

    private float maxMovementPoints = 5;
    private Movable movable;
    private EnemyBehavior behavior;

    public List<HexCoordinates> patrolRouteWaypoints;
    private int patrolRouteIndex = 0;

    void Start() {
        movementPoints = maxMovementPoints;
        var hexGrid = GameObject.Find("Hex Grid").GetComponent<HexGrid>();
        movable = GetComponent<Movable>();
        movable.setCallback(hex => {
            coordinates = hex.coordinates;
            movementPoints--; // TODO decrease by movt cost of hex 
        });
    }

    void Update() {}

    public void setPatrolRoute(IEnumerable<HexCoordinates> waypoints) {
        if (patrolRouteWaypoints == null) {
            patrolRouteWaypoints = new List<HexCoordinates>();
        }
        foreach (var wp in waypoints) {
            patrolRouteWaypoints.Add(wp);
        }
    }

    public HexCoordinates getPatrolWaypoint() {
        if (patrolRouteWaypoints.Count == 0) {
            return null;
        }
        if (coordinates.Equals(patrolRouteWaypoints[patrolRouteIndex])) {
            patrolRouteIndex++;
            if (patrolRouteIndex >= patrolRouteWaypoints.Count) {
                patrolRouteIndex = 0;
            }
        }
        return patrolRouteWaypoints[patrolRouteIndex];
    }

    public async Task doTurn() {
        await behavior.doTurn();
    }

    public async Task<bool> move(HexCell[] path, Func<HexCoordinates, bool> cancelCallback) {
        return await GetComponent<Movable>().MoveAlongPath(path, false, cancelCallback);
    }

    public void resetMovementPoints() {
        movementPoints = maxMovementPoints;
    }

    public void setBehavior(EnemyBehavior behavior) {
        this.behavior = behavior;
    }

    public bool isMoving() { // TODO remove this
        return false;
    }
}
