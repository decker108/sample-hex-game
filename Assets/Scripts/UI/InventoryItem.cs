using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class InventoryItem : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler {
    public ItemStats stats;
    public bool isPickedUp;
    public bool isEquipped;

    private Coroutine popupCoroutine;

    void OnEnable() {
        isPickedUp = false;
        isEquipped = false;
    }

    void Update() {
        if (isPickedUp) {
            transform.position = Input.mousePosition;
        }
    }

    public void setImage(Sprite sprite) {
        GetComponent<Image>().sprite = sprite;
    }

    public void render() {
        var (width, height) = stats.size;
        var (x, y) = stats.position;

        GetComponent<Image>().rectTransform.sizeDelta = new Vector2(
            width * ItemStats.SIZE_IN_PIXELS, 
            height * ItemStats.SIZE_IN_PIXELS);
        // Zero local position
        var position = new Vector3(transform.localPosition.x - 200,
            transform.localPosition.y + 150, 0);
        // Adjust position based on stats.size
        position.x += ((float) width * ItemStats.SIZE_IN_PIXELS) / 2f;
        position.y -= ((float) height * ItemStats.SIZE_IN_PIXELS) / 2f;
        // Adjust position based on stats.position
        position.x += (float) x * ItemStats.SIZE_IN_PIXELS;
        position.y -= (float) y * ItemStats.SIZE_IN_PIXELS;

        transform.localPosition = position;
    }

    public void OnPointerClick(PointerEventData e) {
        if (e.button == PointerEventData.InputButton.Left) {
            var handler = GetComponentInParent<InventoryItemsHandler>();
            if (!isPickedUp && handler.getDraggedItem() == null) {
                handleDrag();
            } else {
                Debug.Log("Slot occupied by other item, cannot drop here");
                transform.parent.parent.parent.Find("ToastPanel").GetComponent<UIToast>()
                    .showMessage("Item is blocked!", UIToast.ToastType.WARNING);
            }
        } else if (e.button == PointerEventData.InputButton.Right) {
            // open item detail view
            var popup = transform.parent.parent.parent.Find("ItemDetailPopup");
            popup.gameObject.SetActive(true);
            popup.GetComponent<ItemDetailHandler>().showPopup(this.stats);
        }
    }

    public void OnPointerEnter(PointerEventData eventData) {
        var handler = GetComponentInParent<InventoryItemsHandler>();
        var draggedItem = handler.getDraggedItem();
        if (draggedItem != null) {
            var placer = getPlacerByParent(transform.parent.name);
            var currentSlot = placer.getSlotByPosition(stats.position);
            var hoveredArea = placer.findNeighbors(draggedItem.stats.size, currentSlot);
            var neighbors = placer.findNeighbors(stats.size, currentSlot);
            hoveredArea.ForEach(s => s.markValid());
            neighbors.ForEach(s => s.markInvalid());
        } else {
            // start countdown to popup
            popupCoroutine = StartCoroutine(showInfoPopup(stats));
        }
    }

    public void OnPointerExit(PointerEventData eventData) {
        var handler = GetComponentInParent<InventoryItemsHandler>();
        var item = handler.getDraggedItem();
        if (item != null) {
            var placer = getPlacerByParent(transform.parent.name);
            var neighbors = placer.findNeighbors(stats.size, placer.getSlotByPosition(stats.position));
            neighbors.ForEach(s => s.markDefault());
        } else {
            if (popupCoroutine != null) {
                closeInfoPopup();
                StopCoroutine(popupCoroutine);
            }
        }
    }

    public void handleDrag() {
        // mark item as picked up
        isPickedUp = true;
        // disable raycast target so that underlying inventory slots can be targeted
        GetComponent<Image>().raycastTarget = false;
        GetComponent<Image>().color = new Color(1, 1, 1, 0.5f); // increase transparency
    }

    public void handleDrop(InventorySlot center, List<InventorySlot> slots) {
        // find closest slots and snap to them
        var (x1,y1) = stats.position;
        stats.position = center.getSlotPosition();
        var (x2,y2) = stats.position;

        // unmark item as picked up
        isPickedUp = false;
        GetComponent<Image>().raycastTarget = true;
        GetComponent<Image>().color = new Color(1, 1, 1, 1); //reset transparency

        if (center.transform.parent.name == "ItemContainerLeftGridPanel") {
            // either moved from inventory to container or just inside container
            var contItems = transform.parent.parent.Find("ContainerItems");
            if (transform.parent != contItems) {
                Debug.Log($"Setting parent of {this.name} to {contItems.name}");
                transform.SetParent(contItems);
            }
        } else {
            // either moved from container to inventory or just inside inventory
            var invItems = transform.parent.parent.Find("InventoryItems");
            if (transform.parent != invItems) {
                Debug.Log($"Setting parent of {this.name} to {invItems.name}");
                transform.SetParent(invItems);
                PlayerSearchable grid = Transform.FindObjectOfType<TurnManager>();;
                grid.getActivePlayer().getInventory().Add(stats);
            }
        }

        // center item texture position in slot
        var (width, height) = stats.size;
        // begin from destination slot local position
        var position = center.transform.localPosition;
        // shift right and down by half the object size
        position.x += ((width/2f) * ItemStats.SIZE_IN_PIXELS);
        position.y -= ((height/2f) * ItemStats.SIZE_IN_PIXELS);
        // shift left and up by half the slot size
        position.x -= InventorySlot.PX_SIZE / 2;
        position.y += InventorySlot.PX_SIZE / 2;
        transform.localPosition = position;

        // reset underlying slot highlighting
        slots.ForEach(s => s.markDefault());
    }

    public void setClickable(bool state) {
        GetComponent<Image>().raycastTarget = state;
    }

    private InventorySlotPlacer getPlacerByParent(string parentName) {
        var placersArr = transform.parent.parent.GetComponentsInChildren<InventorySlotPlacer>();
        var placers = new List<InventorySlotPlacer>(placersArr);
        if (placers.Count == 1) {
            return placers[0];
        }
        if (parentName == "InventoryItems") {
            return placers.Find(p => p.name == "ItemContainerRightGridPanel");
        } else {
            return placers.Find(p => p.name == "ItemContainerLeftGridPanel");
        }
    }

    IEnumerator showInfoPopup(ItemStats stats) {
        yield return new WaitForSecondsRealtime(0.8f);
        var popup = transform.parent.parent.parent.Find("ItemComparisonPopup");
        popup.gameObject.SetActive(true);
        // TODO replace with real stats once implemented
        var text = "Example";
        popup.GetComponentInChildren<ItemComparisonPopupHandler>().init(text);
    }

    private void closeInfoPopup() {
        var popup = transform.parent.parent.parent.Find("ItemComparisonPopup");
        popup.gameObject.SetActive(false);
    }
}