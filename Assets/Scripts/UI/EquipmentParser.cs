using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;
using SimpleJSON;
using static ItemStats;

public class EquipmentParser {

    /** Returns a tuple where the first value is all possible items and the second is player starting equipment **/
    public static (List<ItemStats>,List<(BodySlot,ItemStats)>) parseItemsFile(string filePath) {
        string json = File.ReadAllText(filePath);
        var blob = JSON.Parse(json);

        var items = new List<ItemStats>();
        var parsedItems = blob["items"].Values;
        foreach (var parsedItem in parsedItems) {
            var id = parsedItem.GetValueOrDefault("id", -1).AsInt;
            var name = parsedItem.GetValueOrDefault("name", "MISSING");
            var cost = parsedItem.GetValueOrDefault("cost", 0).AsInt;
            var textureName = parsedItem.GetValueOrDefault("textureName", "MISSING");
            var sizeStr = (string) parsedItem.GetValueOrDefault("size", "1,1");
            var bodySlotStr = parsedItem.GetValueOrDefault("bodySlot", "NONE");
            var description = parsedItem.GetValueOrDefault("description", "Empty description");
            var typeStr = parsedItem.GetValueOrDefault("type", "OTHER");
            var effects = parsedItem.GetValueOrDefault("effects", null);
            var detailTextureName = parsedItem.GetValueOrDefault("detailTextureName", null);
            var modelName = parsedItem.GetValueOrDefault("modelName", null);
            var type = (ItemStats.Type) Enum.Parse(typeof(ItemStats.Type), typeStr);
            var size = (int.Parse(sizeStr.Split(',')[0]), int.Parse(sizeStr.Split(',')[1]));
            var position = (-1,-1);
            // TODO handle equipment equippable in multiple places (e.g. rings)
            var bodySlot = (BodySlot) Enum.Parse(typeof(BodySlot), bodySlotStr);
            var item = new ItemStats(id, name, cost, textureName, size, position, bodySlot, type, effects, modelName);
            item.description = description;
            if (detailTextureName != null) {
                item.detailTextureName = detailTextureName;
            }
            items.Add(item);
        }

        var startingEquipment = new List<(BodySlot,ItemStats)>();
        parsedItems = blob["startingEquipment"].Values;
        foreach (var parsedItem in parsedItems) {
            var id = parsedItem.GetValueOrDefault("id", -1).AsInt;
            var posStr = (string) parsedItem.GetValueOrDefault("position", null);
            var bodySlotStr = parsedItem.GetValueOrDefault("equippedSlot", "NONE");
            
            var item = new ItemStats(items.Find(i => i.id == id));
            if (posStr == null || !bodySlotStr.Equals("NONE")) {
                var bodySlot = (BodySlot) Enum.Parse(typeof(BodySlot), bodySlotStr);
                
                startingEquipment.Add((bodySlot, item));
            } else {
                var pos = (int.Parse(posStr.Split(',')[0]), int.Parse(posStr.Split(',')[1]));
                item.position = pos;
                startingEquipment.Add((BodySlot.NONE, item));
            }
        }

        return (items, startingEquipment);
    }
}
