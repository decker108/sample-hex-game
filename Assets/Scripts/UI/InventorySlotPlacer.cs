using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventorySlotPlacer : MonoBehaviour {
    public static readonly int numberOfSlots = 108;
    public static readonly int slotsPerRow = 12;
    public GameObject inventorySlotPrefab;
    public GameObject inventoryItemsContainer;
    public Sprite defaultSprite, validPlacementSprite, invalidPlacementSprite;

    private InventorySlot[] slots = new InventorySlot[numberOfSlots];

    void Start() {
        int x = 0, y = 0;
        for (int i = 0; i < numberOfSlots; i++) {
            if (i > 0 && i % 12 == 0) {
                y++;
                x = 0;
            }
            var gobj = Instantiate<GameObject>(inventorySlotPrefab, transform);
            gobj.name = string.Format("InventorySlot{0,3:000}", i); // pad with 3 zeroes
            var slot = gobj.GetComponent<InventorySlot>();
            slots[i] = slot;
            slot.x = x++;
            slot.y = y;
        }
        InventorySlot.setSprites(defaultSprite, validPlacementSprite, invalidPlacementSprite);
    }

    public List<InventorySlot> findNeighbors((int, int) size, InventorySlot startSlot) {
        if (startSlot == null) {
            return new List<InventorySlot>(0) {};
        }
        if (size.Item1 == 1 && size.Item2 == 1) {
            return new List<InventorySlot>(new InventorySlot[] {startSlot});
        }
        var (x1, y1) = startSlot.getSlotPosition();
        var (width, height) = size;
        var positions = new List<(int, int)>();
        for (int y = y1; y < (y1 + height); y++) {
            for (int x = x1; x < (x1 + width); x++) {
                positions.Add((x, y));
            }
        }
        return getSlotsByPositions(positions);
    }

    public List<InventorySlot> getSlotsByPositions(IEnumerable<(int, int)> positions) {
        var matches = new List<InventorySlot>();
        foreach (var slot in slots) {
            var (x,y) = slot.getSlotPosition();
            foreach (var pos in positions) {
                var (x1,y1) = pos;
                if (x == x1 && y == y1) {
                    matches.Add(slot);
                }
            }
        }
        return matches;
    }

    public InventorySlot getSlotByPosition((int, int) position) {
        foreach (var slot in slots) {
            var (x2,y2) = slot.getSlotPosition();
            var (x1,y1) = position;
            if (x2 == x1 && y2 == y1) {
                return slot;
            }
        }
        Debug.LogWarning($"Found no slot for position {position}");
        return null;
    }

    public bool hasCollisions(List<InventorySlot> hoveredArea) {
        return getCollisions(hoveredArea).Count > 0;
    }

    public List<InventorySlot> getCollisions(List<InventorySlot> hoveredArea) {
        var invalidSlots = getAllInvalidSlots();
        if (invalidSlots.Count > 0) {
            return invalidSlots;
        }

        var hoveredAreaPos = new HashSet<(int, int)>(
            hoveredArea.ConvertAll<(int, int)>(x => x.getSlotPosition()));
        var handler = transform.GetComponentInParent<InventoryItemsHandler>();
        var items = handler.getAllItems(inventoryItemsContainer);
        items.Remove(handler.getDraggedItem());
        items.RemoveAll(i => i.stats.position == (-1,-1));
        foreach (var item in items) {
            var startSlot = getSlotByPosition(item.stats.position);
            var neighbors = findNeighbors(item.stats.size, startSlot);
            var itemAreaPos = neighbors.ConvertAll<(int, int)>(x => x.getSlotPosition());
            if (hoveredAreaPos.Overlaps(itemAreaPos)) {
                var overlap = new HashSet<(int, int)>(hoveredAreaPos);
                overlap.IntersectWith(itemAreaPos);
                return getSlotsByPositions(overlap);
            }
        }

        return new List<InventorySlot>() {};
    }

    private List<InventorySlot> getAllInvalidSlots() {
        var matches = new List<InventorySlot>();
        foreach (var slot in slots) {
            if (slot.GetComponent<Image>().mainTexture == invalidPlacementSprite) {
                matches.Add(slot);
            }
        }
        return matches;
    }

    public void resetHighlighting() {
        // iterate through all slots and call reset func
        foreach (var slot in slots) {
            slot.markDefault();
        }
    }

    void Update() {}
}
