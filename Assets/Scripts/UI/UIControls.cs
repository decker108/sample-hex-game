using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class UIControls : MonoBehaviour {
    private const int SPELLBOOK_INDEX = 4; // TODO replace with transform.Find("...")
    private const int INVENTORY_INDEX = 5;

    private int spellbookPage = 0;
    private UIToast toast;
    private TreasureManager treasureMgr;

    void Start() {
        var turnCtr = transform.parent.Find("TimePanel").Find("TurnCounter").GetComponent<Text>();
        turnCtr.text = "1";

        toast = transform.parent.Find("ToastPanel").GetComponent<UIToast>();
        treasureMgr = Transform.FindObjectOfType<TreasureManager>();
    }

    void Update() {
        if (Input.GetKeyUp(KeyCode.I)) {
            handleInventoryOpen();
        }
        if (Input.GetKeyUp(KeyCode.B)) {
            handleSpellbookOpen();
        }
        // if (Input.GetKeyUp(KeyCode.U)) { // TODO for testing only, remove later
        //     handleItemContainerOpen();
        // }
        if (Input.GetKeyUp(KeyCode.End)) {
            endTurnClick();
        }
    }

    public void handleSpellbookOpen() {
        var spellbookPanel = transform.parent.GetChild(SPELLBOOK_INDEX).gameObject;
        if (spellbookPanel.activeSelf) {
            spellbookPanel.SetActive(false);
        } else {
            spellbookPanel.SetActive(true);
            populateSpellbookPage(spellbookPanel);
        }
    }

    public void handleSpellbookClose() {
        transform.parent.GetChild(SPELLBOOK_INDEX).gameObject.SetActive(false);
    }

    public void handleSpellbookRightArrowClick() {
        if (spellbookPage + 1 < GlobalGameData.allSpells.Count) {
            spellbookPage++;
            var spellbookPanel = transform.parent.GetChild(SPELLBOOK_INDEX).gameObject;
            populateSpellbookPage(spellbookPanel);
        }
    }

    public void handleSpellbookLeftArrowClick() {
        if (spellbookPage - 1 >= 0) {
            spellbookPage--;
            var spellbookPanel = transform.parent.GetChild(SPELLBOOK_INDEX).gameObject;
            populateSpellbookPage(spellbookPanel);
        }
    }

    public void handleSpellIconClick() {
        PlayerSearchable turnManager = Transform.FindObjectOfType<TurnManager>();
        turnManager.getActivePlayer().activeSpell = GlobalGameData.allSpells[spellbookPage];
        handleSpellbookClose();
    }

    public void handleInventoryOpen() {
        var inventoryPanel = transform.parent.GetChild(INVENTORY_INDEX).gameObject;
        if (inventoryPanel.activeSelf) {
            inventoryPanel.SetActive(false);
        } else {
            inventoryPanel.SetActive(true);
            var handler = Transform.FindObjectOfType<InventoryItemsHandler>();
            handler.render();
        }
    }

    public void handleInventoryClose() {
        transform.parent.GetChild(INVENTORY_INDEX).gameObject.SetActive(false);
    }

    public async void endTurnClick() {
        TurnManager turnManager = Transform.FindObjectOfType<TurnManager>();
        var player = turnManager.getActivePlayer();
        await turnManager.endPlayerTurn(player);
    }

    public void handleOptionsClick() {
        Debug.Log("Clicked options button");
    }

    public void handleItemContainerOpen() {
        var itemContainerPanel = transform.parent.Find("ItemContainerPanel").gameObject;
        if (itemContainerPanel.activeSelf) {
            itemContainerPanel.SetActive(false);
        } else {
            itemContainerPanel.SetActive(true);
            var itemsObj = itemContainerPanel.transform.Find("ContainerItems");
            var itemsHandler = itemContainerPanel.GetComponent<InventoryItemsHandler>();
            itemsHandler.updateInventoryPlacement(treasureMgr.getMinorTreasure(), itemsObj);
        }
    }

    public void handleItemContainerClose() {
        var itemContainerPanel = transform.parent.Find("ItemContainerPanel").gameObject;
        itemContainerPanel.SetActive(false);
    }

    public void handleEndOfTurnVisuals(int turn) {
        transform.parent.Find("TimePanel").Find("TurnCounter").GetComponent<Text>().text = $"{turn}";
        toast.showMessage($"Turn {turn}");
    }

    private void populateSpellbookPage(GameObject spellbookPanel) {
        var textComponents = spellbookPanel.GetComponentsInChildren<TextMeshProUGUI>();
        textComponents[0].text = GlobalGameData.allSpells[spellbookPage].name;
        textComponents[1].text = GlobalGameData.allSpells[spellbookPage].description;

        var leftBtn = spellbookPanel.transform.Find("LeftArrowButton");
        var rightBtn = spellbookPanel.transform.Find("RightArrowButton");
        if (spellbookPage == 0) {
            leftBtn.GetComponent<Button>().interactable = false;
        } else {
            leftBtn.GetComponent<Button>().interactable = true;
        }
        if (spellbookPage == GlobalGameData.allSpells.Count - 1) {
            rightBtn.GetComponent<Button>().interactable = false;
        } else {
            rightBtn.GetComponent<Button>().interactable = true;
        }
    }
}
