using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Events;
using System.Threading.Tasks;
using TMPro;
using UnityEngine.UI;

public class BattlePanelHandler {
    public static void initialize(GameObject panel, GameObject loadingScreen, int number, string enemies, Player player) {
        GameObject.DontDestroyOnLoad(loadingScreen);
        panel.SetActive(true);
        panel.transform.GetChild(1).GetComponent<TMP_Text>().text = getDescription(number, enemies);
        panel.transform.GetChild(2).GetComponent<Button>().onClick.AddListener(createOnAttackClickListener(loadingScreen, player));
        panel.transform.GetChild(3).GetComponent<Button>().onClick.AddListener(createOnCancelClickListener(panel));
    }

    private static UnityAction createOnCancelClickListener(GameObject panel) {
        return () => {
            panel.SetActive(false);
        };
    }

    private static UnityAction createOnAttackClickListener(GameObject loadingScreen, Player player) {
        return async () => {
            var loadingPanel = loadingScreen.transform.GetChild(0).gameObject;
            loadingPanel.SetActive(true);
            GlobalGameData.combatInitiatorIdx = GlobalGameData.players.FindIndex(0, p => p == player);
            AsyncOperation loadingScene = SceneManager.LoadSceneAsync("Scenes/BattleScene");
            while (!loadingScene.isDone) {
                await Task.Delay(100);
            }
            loadingPanel.SetActive(false);
            GameObject.Destroy(loadingScreen);
        };
    }

    private static string getDescription(int number, string enemies) {
        return $"A group of {number} {enemies} can be seen in the distance. What do you do?";
    }
}