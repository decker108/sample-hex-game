using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class WearableSlot : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler {
    public ItemStats.BodySlot bodySlot;
    public InventoryItem equippedItem;
    private Image m_Image;
    private static Sprite defaultSprite, validPlacementSprite, invalidPlacementSprite;

    void Start() {
        m_Image = GetComponent<Image>();
    }

    public void OnPointerEnter(PointerEventData eventData) {
        var handler = transform.parent.GetComponentInParent<InventoryItemsHandler>();
        var item = handler.getDraggedItem();
        if (item != null) {
            if (item.stats.bodySlot == bodySlot) {
                markValid();
            } else {
                markInvalid();
            }
        }
    }

    public void OnPointerExit(PointerEventData eventData) {
        var handler = transform.parent.GetComponentInParent<InventoryItemsHandler>();
        var item = handler.getDraggedItem();
        if (item != null) {
            markDefault();
        }
    }

    public void OnPointerClick(PointerEventData eventData) {
        var handler = transform.parent.GetComponentInParent<InventoryItemsHandler>();
        var draggedItem = handler.getDraggedItem();
        if (draggedItem != null) {
            if (draggedItem.stats.bodySlot == bodySlot && equippedItem == null) {
                markDefault();
                equipItem(draggedItem);
                PlayerSearchable grid = Transform.FindObjectOfType<TurnManager>();;
                grid.getActivePlayer().equipItem(draggedItem.stats, bodySlot);
                // TODO handle previously equipped item here
                equippedItem = draggedItem;
            } else {
                Debug.Log($"Cannot equip item {draggedItem.stats.name} ({draggedItem.stats.bodySlot}) on {bodySlot}");
            }
        } else {
            unequipItem(equippedItem);
            equippedItem = null;
        }
    }

    public static void setSprites(Sprite _defaultSprite, Sprite valid, Sprite invalid) {
        defaultSprite = _defaultSprite;
        validPlacementSprite = valid;
        invalidPlacementSprite = invalid;
    }

    public void markInvalid() {
        m_Image.sprite = invalidPlacementSprite;
    }

    public void markValid() {
        m_Image.sprite = validPlacementSprite;
    }

    public void markDefault() {
        m_Image.sprite = defaultSprite;
    }

    // visible for setting up starting equipment
    public void equipItem(InventoryItem item) {
        if (item == null) {
            Debug.Log("No held item to equip, skipping");
            return;
        }
        item.stats.position = (-1, -1);
        item.isPickedUp = false;
        item.isEquipped = true;

        item.GetComponent<Image>().color = new Color(1, 1, 1, 1);

        // resize image to 32x32
        item.GetComponent<Image>().rectTransform.sizeDelta = new Vector2(32, 32);

        // position item in slot
        item.transform.position = transform.position;
    }

    private void unequipItem(InventoryItem item) {
        if (item == null) {
            Debug.Log("No item equipped, skipping");
            return;
        }
        PlayerSearchable grid = Transform.FindObjectOfType<TurnManager>();
        grid.getActivePlayer()
            .unequipItem(item.stats, bodySlot);
        item.isPickedUp = true;
        item.isEquipped = false;

        item.GetComponent<Image>().color = new Color(1, 1, 1, 0.5f);

        // resize image to original item size
        var (width, height) = item.stats.size;
        item.GetComponent<Image>().rectTransform.sizeDelta = new Vector2(
            width * InventorySlot.PX_SIZE, height * InventorySlot.PX_SIZE);
        
        // position item in slot
        item.transform.position = Input.mousePosition;
    }
}
