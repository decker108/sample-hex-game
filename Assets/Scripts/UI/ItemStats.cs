using System;

public class ItemStats : ICloneable {
    public enum BodySlot {
        NONE,
        HEAD, 
        NECK, 
        TORSO, 
        LEFT_ARM, 
        RIGHT_ARM, 
        LEFT_HAND_FINGER, 
        RIGHT_HAND_FINGER, 
        FEET, 
        WAIST,
    }

    public enum Type {
        OTHER,
        CONSUMABLE,
        MELEE_WEAPON,
        RANGED_WEAPON,
        CLOTHING,
        SHIELD,
        RING,
    }

    public const float SIZE_IN_PIXELS = 32f;

    public int id;
    public string name;
    public int cost;
    public string textureName;

    public (int, int) size;
    public (int, int) position;

    public BodySlot bodySlot;
    public Type type;
    public object[] effects; // TODO replace with proper data type
    public string description;
    public string detailTextureName;
    public string modelName;

    public ItemStats() {
        id = 1;
        name = "Example";
        cost = 1;
        textureName = "health_potion";
        size = (1,1);
        position = (0,0);
        bodySlot = BodySlot.NONE;
        description = "Empty description";
    }

    public ItemStats(int id, string name, int cost, string textureName, (int,int) size, (int,int) position, BodySlot bodySlot, Type type, object[] effects, string modelName) {
        this.id = id;
        this.name = name;
        this.cost = cost;
        this.textureName = textureName;
        this.size = size;
        this.position = position;
        this.bodySlot = bodySlot;
        this.type = type;
        this.effects = effects;
        this.description = "";
        this.modelName = modelName;
    }

    public ItemStats(ItemStats original) {
        this.id = original.id;
        this.name = original.name;
        this.cost = original.cost;
        this.textureName = original.textureName;
        this.size = original.size;
        this.position = original.position;
        this.bodySlot = original.bodySlot;
        this.type = original.type;
        this.effects = original.effects;
        this.description = original.description;
        this.modelName = original.modelName;
    }

    public void setSize(int width, int height) {
        size = (width, height);
    }

    public void setInventoryPosition(int x, int y) {
        position = (x, y);
    }

    public override string ToString() {
        var str = "" +
        $"id={this.id} " +
        $"name={this.name} " +
        $"cost={this.cost} " +
        $"textureName={this.textureName} " +
        $"size={this.size} " +
        $"position={this.position} " +
        $"bodySlot={this.bodySlot} " +
        $"type={this.type} " +
        $"effects={this.effects} " +
        $"description={this.description} " +
        $"modelName={this.modelName} ";
        return str;
    }

    public object Clone() {
        return new ItemStats(
            this.id,
            this.name,
            this.cost,
            this.textureName,
            this.size,
            this.position,
            this.bodySlot,
            this.type,
            this.effects,
            // this.description,
            this.modelName
        );
    }
}