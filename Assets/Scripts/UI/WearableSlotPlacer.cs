using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using static ItemStats;

// Creates body slots and sets background sprites
public class WearableSlotPlacer : MonoBehaviour {
    public Sprite defaultSprite, validPlacementSprite, invalidPlacementSprite;

    void OnEnable() {
        WearableSlot.setSprites(defaultSprite, validPlacementSprite, invalidPlacementSprite);
        setBodySlots(GetComponentsInChildren<WearableSlot>());
    }

    private void setBodySlots(WearableSlot[] wearableSlots) {
        foreach (var slot in wearableSlots) {
            BodySlot bodySlot;
            switch (slot.gameObject.name) {
                case "TorsoSlot": {bodySlot = BodySlot.TORSO; break;}
                case "LeftArmSlot": {bodySlot = BodySlot.LEFT_ARM; break;}
                case "RightArmSlot": {bodySlot = BodySlot.RIGHT_ARM; break;}
                case "FeetSlot": {bodySlot = BodySlot.FEET; break;}
                case "RightHandRingSlot": {bodySlot = BodySlot.RIGHT_HAND_FINGER; break;}
                case "LeftHandRingSlot": {bodySlot = BodySlot.LEFT_HAND_FINGER; break;}
                case "HeadSlot": {bodySlot = BodySlot.HEAD; break;}
                case "WaistSlot": {bodySlot = BodySlot.WAIST; break;}
                case "NeckSlot": {bodySlot = BodySlot.NECK; break;}
                default: {bodySlot = BodySlot.NONE; break;}
            }
            slot.bodySlot = bodySlot;
        }
    }

    public WearableSlot getWearableSlotByBodySlot(BodySlot bodySlot) {
        var slots = GetComponentsInChildren<WearableSlot>();
        foreach (var slot in slots) {
            if (slot.bodySlot == bodySlot) {
                return slot;
            }
        }
        Debug.Log("Could not find matching slot");
        return null;
    }
}
