using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemComparisonPopupHandler : MonoBehaviour {
    // Used for positioning popup at lower left of cursor
    private Vector3 offset = new Vector3(110, -120, 0); // x = width/2+10, y = -height/2-20

    void Start() {
        transform.position = Input.mousePosition + offset;
    }

    void Update() {
        transform.position = Input.mousePosition + offset;
    }

    public void init(string text) {
        transform.position = Input.mousePosition + offset;
        GetComponentInChildren<TMPro.TMP_Text>().text = text;
    }
}
