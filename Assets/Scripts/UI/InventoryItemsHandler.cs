using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.AddressableAssets;

public class InventoryItemsHandler : MonoBehaviour {
    public GameObject inventoryItemPrefab;
    public bool isDraggingItem;
    public TurnManager turnManager;
    private Transform invItemsPanel;

    void Start() {
        isDraggingItem = false;
    }

    public void render() {
        invItemsPanel = transform.Find("InventoryItems");
        clearInventoryAndBodySlots(invItemsPanel);
        populateInventory(invItemsPanel);
        populateBodySlots(invItemsPanel);
    }

    private void populateInventory(Transform invItemsPanel) {
        var player = turnManager.getActivePlayer();
        var playerItems = player.getInventory();
        foreach (var item in playerItems) {
            var invItem = createInventoryItem(item, invItemsPanel);
            invItem.render();
        }
    }

    private void populateBodySlots(Transform invItemsPanel) {
        var placer = GetComponentInChildren<WearableSlotPlacer>();
        if (placer != null) { // this means we're in a shop/container view
            var equippedItems = turnManager.getActivePlayer().getEquippedItems();
            foreach (var (bodySlot, item) in equippedItems) {
                var invItem = createInventoryItem(item, invItemsPanel);
                invItem.render();
                invItem.setClickable(false);
                var slot = placer.getWearableSlotByBodySlot(bodySlot);
                slot.equipItem(invItem);
                slot.equippedItem = invItem;
            }
        }
    }

    public void updateInventoryPlacement(List<ItemStats> items, Transform containerItemsPanel) {
        var invItemsPanel = transform.Find("InventoryItems");
        clearInventoryAndBodySlots(invItemsPanel);
        foreach (var item in items) {
            var invItem = createInventoryItem(item, containerItemsPanel);
            invItem.render();
        }
    }

    public InventoryItem getDraggedItem() {
        var inventoryItems = 
            transform.Find("InventoryItems").GetComponentsInChildren<InventoryItem>(true);
        var containerItems = 
            transform.Find("ContainerItems")?.GetComponentsInChildren<InventoryItem>(true) ?? Array.Empty<InventoryItem>();
        var items = new List<InventoryItem>();
        items.AddRange(inventoryItems);
        items.AddRange(containerItems);
        foreach (var item in items) {
            if (item.isPickedUp) {
                isDraggingItem = true;
                return item;
            }
        }
        isDraggingItem = false;
        return null;
    }

    public List<InventoryItem> getAllItems(GameObject inventoryItemsContainer) {
        return new List<InventoryItem>(
            inventoryItemsContainer.GetComponentsInChildren<InventoryItem>(true));
    }

    private InventoryItem createInventoryItem(ItemStats item, Transform invItems) {
        var gobj = Instantiate(inventoryItemPrefab, Vector3.zero, 
                transform.rotation, invItems);
        gobj.name = item.name;
        gobj.transform.localPosition = Vector3.zero;
        var invItem = gobj.GetComponent<InventoryItem>();
        invItem.stats = item;
        // Note: rebuild assets when changed
        Addressables.LoadAssetAsync<Sprite>($"Assets/Textures/items/{item.textureName}.png")
            .Completed += (r) => invItem.setImage(r.Result);
        return invItem;
    }

    private void clearInventoryAndBodySlots(Transform invItemsPanel) {
        var currentItems = 
            invItemsPanel.GetComponentsInChildren<InventoryItem>(true);
        for (int i = 0; i < currentItems.Length; i++) {
            Destroy(currentItems[i].gameObject);
            currentItems[i] = null;
        }
    }
}
