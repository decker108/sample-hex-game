using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class InventorySlot : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler {
    public const float PX_SIZE = 32;
    public int x, y;
    private Image m_Image;
    private static Sprite defaultSprite, validPlacementSprite, invalidPlacementSprite;

    void Start() {
        m_Image = GetComponent<Image>();
    }

    void Update() {}

    public void OnPointerEnter(PointerEventData eventData) {
        var placer = GetComponentInParent<InventorySlotPlacer>();
        // if object is being dragged, color slots green if object can be dropped, otherwise red
        // find out which object (if any) is being dragged
        var handler = transform.parent.parent.GetComponent<InventoryItemsHandler>();
        InventoryItem item = handler.getDraggedItem();
        if (item != null) {
            placer.resetHighlighting();
            var neighbors = placer
                .findNeighbors(item.stats.size, this);
            var collisions = placer.getCollisions(neighbors);
            if (collisions.Count > 0) {
                neighbors.ForEach(s => s.markValid());
                collisions.ForEach(s => s.markInvalid());
            } else {
                neighbors.ForEach(s => s.markValid());
                this.markValid(); // Is this needed?
            }
        }
    }

    public void OnPointerExit(PointerEventData eventData) {
        var handler = transform.parent.parent.GetComponent<InventoryItemsHandler>();
        InventoryItem item = handler.getDraggedItem();
        if (item != null) {
            // if object is being dragged, reset slot texture
            m_Image.sprite = defaultSprite;
            var placer = GetComponentInParent<InventorySlotPlacer>();
            placer.resetHighlighting();
        }
    }

    public static void setSprites(Sprite _defaultSprite, Sprite valid, Sprite invalid) {
        defaultSprite = _defaultSprite;
        validPlacementSprite = valid;
        invalidPlacementSprite = invalid;
    }

    public void OnPointerClick(PointerEventData e) {
        var handler = transform.parent.parent.GetComponent<InventoryItemsHandler>();
        InventoryItem item = handler.getDraggedItem();
        if (item != null) {
            // TODO handle moving items between item containers (switch parent gobj)
            var placer = GetComponentInParent<InventorySlotPlacer>();
            var neighbors = placer
                .findNeighbors(item.stats.size, this);
            var totalCoveredSlots = item.stats.size.Item1 * item.stats.size.Item2;
            if (placer.getCollisions(neighbors).Count > 0 || neighbors.Count < totalCoveredSlots) {
                Debug.Log("Slot occupied by other item, cannot drop here");
                transform.parent.parent.parent.Find("ToastPanel").GetComponent<UIToast>()
                    .showMessage("Not enough inventory space!", UIToast.ToastType.WARNING);
            } else {
                item.handleDrop(this, neighbors);
            }
        }
    }

    public void markInvalid() {
        m_Image.sprite = invalidPlacementSprite;
    }

    public void markValid() {
        m_Image.sprite = validPlacementSprite;
    }

    public void markDefault() {
        m_Image.sprite = defaultSprite;
    }

    public (int, int) getSlotPosition() {
        return (x, y);
    }
}
