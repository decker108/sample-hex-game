using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIDraggable : MonoBehaviour {
    bool startDrag;
   
    void Update() {
        if (startDrag) {
            transform.position = Input.mousePosition;
        }
    }

    public void StartDragUI() {
        Debug.Log("Called StartDragUI");
        startDrag = true;
    }

    public void StopDragUI() {
        Debug.Log("Called StopDragUI");
        startDrag = false;
    }
}
