using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AddressableAssets;

public class ItemDetailHandler : MonoBehaviour {
    public GameObject itemName;
    public GameObject itemImage;
    public GameObject itemDescr;

    void Start() {
    }

    void Update() {
    }

    public void showPopup(ItemStats item) {
        Debug.Log("Opening item detail popup");
        itemName.GetComponent<TMPro.TMP_Text>().text = item.name;
        itemDescr.GetComponent<TMPro.TMP_Text>().text = item.description;
        var path = item.detailTextureName != null ?
            $"Assets/Textures/items/detail/{item.detailTextureName}.png" :
            $"Assets/Textures/items/{item.textureName}.png";
        Addressables.LoadAssetAsync<Sprite>(path)
            .Completed += (r) => itemImage.GetComponent<Image>().sprite = r.Result;
        // TODO does image need to be resized?
    }

    public void closePopup() {
        this.gameObject.SetActive(false);
    }
}
