using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIToast : MonoBehaviour {
    public enum ToastType {
        INFO, WARNING, CELEBRATORY
    }

    private Image background;
    private GameObject message;

    void Start() {
        background = GetComponent<Image>();
        message = transform.GetChild(0).gameObject;
    }

    void Update() {}

    public void showMessage(string msg, ToastType type = ToastType.INFO, float duration = 3f) {
        StartCoroutine(showMessageFn(msg, type, duration));
    }

    IEnumerator showMessageFn(string msg, ToastType type, float duration) {
        message.SetActive(true);
        background.color = new Color(
            background.color.r, background.color.g, background.color.b, 1f);
        var textObj = message.GetComponent<TextMeshProUGUI>();
        textObj.text = msg;
        if (type == ToastType.WARNING) {
            textObj.color = new Color(1, 0, 0, 1);
        } else {
            textObj.color = new Color(0, 0, 0, 1);
        }
        yield return new WaitForSecondsRealtime(duration);
        message.SetActive(false);
        background.color = new Color(
            background.color.r, background.color.g, background.color.b, 0f);
    }
}
