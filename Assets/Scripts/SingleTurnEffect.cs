using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SingleTurnEffect : SpellEffect {
    private Action<bool> activationEffect;
    private Action<bool> deactivationEffect;

    public SingleTurnEffect(Action<bool> activation, Action<bool> deactivation) {
        this.activationEffect = activation;
        this.deactivationEffect = deactivation;
    }

    public bool isMultiTurn() {
        return false;
    }

    public Action<bool> getActivationEffect() {
        return activationEffect;
    }

    public Action<bool> getDeactivationEffect() {
        return deactivationEffect;
    }
}
