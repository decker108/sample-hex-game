using System.Collections;
using System.Collections.Generic;

public static class GlobalGameData : object {
    public static List<ItemStats> allItems;
    public static List<(ItemStats.BodySlot,ItemStats)> startingEquipment;
    public static TerrainParser.TerrainMapping terrainMapping;
    public static List<Spell> allSpells;
    public static List<EnemyStats> enemyStats;
    public static List<EnemyStats> enemiesInCombat;
    public static List<Player> players;
    public static int combatInitiatorIdx; // index of player who started combat

    public static void loadEquipmentData() {
        var (allEq, startEq) = EquipmentParser.parseItemsFile("Assets/Scripts/itemsData.json");
		GlobalGameData.allItems = allEq;
        GlobalGameData.startingEquipment = startEq;
    }

    public static void loadTerrainData() {
        // TODO get map file name from a static class to support map rotation 
        terrainMapping = TerrainParser.ParseFile("Assets/Scripts/map1.json");
    }

    public static void loadSpellData() {
        allSpells = SpellParser.parseFile("Assets/Scripts/spellsData.json");
    }

    public static void loadEnemyData() {
        enemyStats = EnemyParser.parseFile("Assets/Scripts/enemies.json");
    }

    public static void setEnemiesInCombat(List<EnemyStats> enemies) {
        enemiesInCombat = enemies;
    }
}
