using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationEventHandler : MonoBehaviour {
    private CombatTurnManager turnManager;

    void Start() {
        turnManager = Transform.FindObjectOfType<CombatTurnManager>();
    }

    void Update() {        
    }

    public async void PrintEvent(string s) {
        // Debug.Log("PrintEvent: " + s + " called at: " + Time.time);
        await turnManager.calculateHitAndDamage(s);
    }
}
