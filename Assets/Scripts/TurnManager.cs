using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Threading.Tasks;

public class TurnManager : MonoBehaviour, PlayerSearchable {
    public static List<MultiTurnEffect> activeSpellEffects;
    public UIControls uiControls;
    public HexGrid hexGrid;
    private static int turn = 1;
    private static bool[] handledPlayers;

    void Start() {
        activeSpellEffects = new List<MultiTurnEffect>();
        handledPlayers = new bool[hexGrid.players.Count];
    }

    void Update() {
    }

    public async Task endPlayerTurn(Player activePlayer) {
        int playerIdx = hexGrid.players.LastIndexOf(activePlayer);
        Debug.Log($"End turn for player {playerIdx}");
        handledPlayers[playerIdx] = true;
        if (new List<bool>(handledPlayers).TrueForAll(e => e)) {
            await endTurn();
        }
    }

    public async Task endTurn() {
        await handleEnemyTurns(hexGrid);
        turn++;
        handleMultiTurnTerrainEffects();
        handleMultiturnSpellEffects();
        uiControls.handleEndOfTurnVisuals(turn);
        hexGrid.players.ForEach(p => p.resetMovementPoints());
        hexGrid.enemies.ForEach(e => e.resetMovementPoints());
        for (int i = 0; i < handledPlayers.Length; i++) {
            handledPlayers[i] = false;
        }
    }

    public static void addMultiTurnEffect(Spell spell) {
        // TODO implement this
        // activeSpellEffects.Add(spell.getActivationEffect(null, null));
    }

    private static void handleMultiturnSpellEffects() {
        foreach (var spellFx in activeSpellEffects) {
            if (spellFx.getDuration() >= 1) {
                spellFx.getActivationEffect().Invoke(true);
            }
            spellFx.setDuration(spellFx.getDuration() - 1);
        }
        activeSpellEffects.RemoveAll((spell) => spell.getDuration() == 0);
    }

    private static void handleMultiTurnTerrainEffects() {
        // TODO implement this
    }

    private async Task handleEnemyTurns(HexGrid hexGrid) {
        foreach (var enemy in hexGrid.enemies) {
            Debug.Log($"{enemy.name} is taking their turn");
            await enemy.doTurn();
            Debug.Log($"{enemy.name} finished their turn");
        }
    }

	public Player getActivePlayer() {
        for (int i = 0; i < handledPlayers.Length; i++) {
            if (handledPlayers[i] == false) {
                return hexGrid.players[i];
            }
        }
        return null;
	}
}
