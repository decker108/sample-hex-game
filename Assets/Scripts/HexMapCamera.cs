using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HexMapCamera : MonoBehaviour
{
    Transform swivel, stick;

    public float moveSpeed = 100f;
	public float zoomLevel = 0f;
	float stickMinZoom = 10; // how far DOWN you can go
	float stickMaxZoom = -50; // how far UP you can go

    public HexGrid grid;

	void Awake () {
		swivel = transform.GetChild(0);
		stick = swivel.GetChild(0);
	}

    void Update () {
        float xDelta = Input.GetAxis("Horizontal");
		float zDelta = Input.GetAxis("Vertical");
		if (xDelta != 0f || zDelta != 0f) {
			AdjustPosition(xDelta, zDelta);
		}

		float zoomDelta = Input.GetAxis("Mouse ScrollWheel");
		if (zoomDelta != 0f) {
			AdjustZoom(zoomDelta);
		}

		if (Input.mousePosition.x <= 5) {
			AdjustPosition(-1f, 0f);
		} else if (Input.mousePosition.x >= Screen.width-5) {
			AdjustPosition(1f, 0f);
		}
		if (Input.mousePosition.y <= 5) {
			AdjustPosition(0f, -1f);
		} else if (Input.mousePosition.y >= Screen.height-5) {
			AdjustPosition(0f, 1f);
		}
    }

	void AdjustZoom (float delta) {
		zoomLevel = Mathf.Clamp01(zoomLevel + delta);

		float distance = Mathf.Lerp(stickMinZoom, stickMaxZoom, zoomLevel);
		stick.localPosition = new Vector3(0f, distance, 0f);
	}

    void AdjustPosition (float xDelta, float zDelta) {
		Vector3 direction = new Vector3(xDelta, 0f, zDelta).normalized; // normalize to ensure same speed for diagonal movement
		float damping = Mathf.Max(Mathf.Abs(xDelta), Mathf.Abs(zDelta)); // dampen movement to remove delays when stopping
		float distance = moveSpeed * damping * Time.deltaTime; // make movement frame rate-independent with deltaTime

		Vector3 position = transform.localPosition;
		position += direction * distance;
        transform.localPosition = ClampPosition(position); // Prevent camera moving outside game area
	}

    Vector3 ClampPosition (Vector3 position) {
        int chunkCountX = 1;
        int chunkSizeX = HexCoordinates.maxX;
        int chunkCountZ = 1;
        int chunkSizeZ = HexCoordinates.maxZ;

        float xMax =
			(chunkCountX * chunkSizeX - 0.5f) *
			(2f * HexMetrics.innerRadius);
		position.x = Mathf.Clamp(position.x, 0f, xMax);

        float zMax =
			(chunkCountZ * chunkSizeZ - 1) *
			(1.5f * HexMetrics.outerRadius);
		position.z = Mathf.Clamp(position.z, 0f, zMax);

		return position;
	}
}
