using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Threading.Tasks;

public class Movable : MonoBehaviour {
    public float movementSpeed = 0;
    public HexCell[] pathToMove;
    private Action<HexCell> updateParentCallback;
    public Animator animator;

    void Start() {
        pathToMove = new HexCell[]{};
    }

    void Update() {}

    public void setCallback(Action<HexCell> updateParentCallback) {
        this.updateParentCallback = updateParentCallback;
    }

    public async Task<bool> MoveAlongPath(HexCell[] path, bool useCache = true,
        Func<HexCoordinates, bool> cancelMoveCallback = null) {
        if (animator != null) {
            animator.SetBool("isWalking", true);
        }
        for (int i = 0; i < path.Length-1; i++) {
            var start = path[i].gameObject.transform.position;
            var end = path[i+1].gameObject.transform.position;
            Vector3 direction = -((start - end).normalized);
            Quaternion lookRotation = Quaternion.LookRotation(direction);
            transform.rotation = lookRotation;
            for (int j = 0; j <= 25; j++) {
                var x = Mathf.Lerp(start.x, end.x, (float)j/25.0f);
                var z = Mathf.Lerp(start.z, end.z, (float)j/25.0f);
                transform.position = new Vector3(x, transform.position.y, z);
                await Task.Delay(TimeSpan.FromSeconds(0.05f));
            }
            updateParentCallback(path[i+1]);
            // Debug.Log($"Arrived at hex {i+1}/{path.Length-1}");
            if (cancelMoveCallback != null && cancelMoveCallback(path[i+1].coordinates)) {
                if (animator != null) {
                    animator.SetBool("isWalking", false);
                }
                return true; // cancelled (movement was interrupted)
            }
        }
        if (useCache) {
            PathFinding.resetCache();
        }
        if (animator != null) {
            animator.SetBool("isWalking", false);
        }
        return false; // not cancelled (movement finished)
    }
}
