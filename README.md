# sample-hex-game

A sample hex-tiled turn-based strategy-RPG made with Unity.

## Setup

* Install Unity 2020 LTS.
* Install vscodium (or vscode).
* Install dotnet core.
* Install mono and related packages.
* Install the C# plugin for vscodium.
* Start Unity and set vscodium to the default editor (you might have to symlink the codium executable to /usr/bin/code).
* Create a script file, right click it and choose Open in C# Project.
* Ensure the project files (.sln and .csproj) are created.
* Done!

## Dependencies

* Addressables
* TextMeshPro
* Shader Graph

## Tools used

* Unity3D 2020.3.8f1
* VSCode (scripts)
* KolourPaint (2D sprites)
* Blender (3D models)
* Tenacity (audio)

## Third party materials

The nightsky skybox texture are used under the Creative Commons CC0 1.0 Universal Public Domain
Dedication.

The daylight skyboxes are made by (clk)dskins and Foe, and are used under
the Creative Commons Attribution-NonCommercial-NoDerivs 4.0 Unported License.